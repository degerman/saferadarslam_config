function LidarData = unpackLidarUdp(data,SS,LidarParam)
%#codegen

% Init Lidar data struct.
LidarData = i_o.struct_LidarData(LidarParam,SS);
% Set time stamp and initialize vectors
Nlayr = LidarParam.NrOfLayers;
Nhorz = LidarParam.NdetLayer;
Necho = LidarParam.NrOfEcho;
% Get new lidar data
if ~isempty(data)
    % Set time stamp
    data = single(data);
    LidarData.fl_CurrTime = single(3600*data(7) + 60*data(8) + data(9) +...
        0.001*(256*data(10) + data(11)));
    for i = 1:Necho
        for j = 1:Nlayr
            for k = 1:Nhorz
                ind = i + Necho*Nhorz*(j-1) + Necho*(k-1);
                idx = (i-1)*Nlayr*Nhorz + Nhorz*(j-1) + k;
                LidarData.au2_Range(idx) = uint16(256*data(40+3*ind) +...
                    data(41+3*ind));
                LidarData.au1_SNR(idx)   = uint8(data(42+3*ind));
            end
        end
    end
end



