classdef Runner<handle
    %Runner and replayer object
    properties
        
        data_param_json
        data_param
        sensor_param_json
        sensor_param
        slam_param_json
        slam_param
   %s     ego_index
        
        jar_file
        adress
        
        sensorQueue
        fusion_engine
        
        poles
        poles_xyz
        visualizer
        GRID
        
        gridmapsender
        
        console_handle
        console_text1
        console_text_handle1
        console_text2
        console_text_handle2
        button1
        button2
        button3
        button4
        button5
        button6
        
        matlab_version % matlab pf filter
    end
    
    methods
        
        function self = Runner(data_param_json,sensor_param_json,slam_param_json,jar_file)
            
            %simple constuctor just storing the json file locations
            self.data_param_json = data_param_json;
            self.sensor_param_json = sensor_param_json;
            self.slam_param_json = slam_param_json;
            
            self.jar_file = jar_file;
            
            
            %setup the figure
            self.console_handle = figure;
            set(self.console_handle,'toolbar','none');
            set(self.console_handle,'menubar','none');
            set(self.console_handle,'units','normalized');
            set(self.console_handle,'Position',[0.0,0.2,0.07,0.60]);
            set(self.console_handle,'numbertitle','off');
           axis off;
                        
%             t = text(-0.1,0.9,cell2mat({'sensor 1: ';'sensor 2: ';'sensor 3: ';'sensor 4: ';'sensor 5: ';'sensor 6: '}));
%             set(t,'FontSize',8);
%             self.console_text1 = {'sec    ';'sec    ';'sec    ';'sec    ';'sec    ';'sec    '};
            self.console_text1 = {'       ';'       ';'       ';'       ';'       ';'       '};
            self.console_text_handle1 = text(-0.4,0.9,cell2mat(self.console_text1));
            set(self.console_text_handle1,'FontSize',8);
%             self.console_text2 = {'mseconds   ';'mseconds   ';'mseconds   ';'mseconds   ';'mseconds   ';'mseconds   '};
            self.console_text2 = {'           ';'           ';'           ';'           ';'           ';'           '};
            self.console_text_handle2 = text(0.3,0.9,cell2mat(self.console_text2));
            set(self.console_text_handle2,'FontSize',8);
            
%             self.button1 = uicontrol('Style', 'pushbutton', 'String', 'Load Sensors', 'FontSize', 6,...
%             'Position', [100 90 100 50],'Callback', 'runner.loadSensorData');       
%             self.button2 = uicontrol('Style', 'pushbutton', 'String', 'Reset Fusion', 'FontSize', 6,...
%             'Position', [220 90 100 50],'Callback', 'runner.initFusion');       
%             self.button3 = uicontrol('Style', 'pushbutton', 'String', 'Add Sensors', 'FontSize', 6,...
%             'Position', [340 90 100 50],'Callback', 'runner.setupSensorsFusion');       
%             self.button4 = uicontrol('Style', 'togglebutton', 'String', 'Mapping', 'FontSize', 6,...
%             'Position', [460 90 100 50],'Callback', 'runner.mapping');       
%             self.button5 = uicontrol('Style', 'togglebutton', 'String', 'Localization', 'FontSize', 6,...
%             'Position', [580 90 100 50],'Callback', 'runner.localization');
            self.button1 = uicontrol('Style', 'pushbutton', 'String', 'Load Sensors', 'FontSize', 6,...
            'Position', [10 400 100 50],'Callback', 'runner.loadSensorData');       
            self.button2 = uicontrol('Style', 'pushbutton', 'String', 'Reset Fusion', 'FontSize', 6,...
            'Position', [10 300 100 50],'Callback', 'runner.initFusion');       
            self.button3 = uicontrol('Style', 'pushbutton', 'String', 'Add Sensors', 'FontSize', 6,...
            'Position', [10 200 100 50],'Callback', 'runner.setupSensorsFusion');       
            self.button4 = uicontrol('Style', 'togglebutton', 'String', 'Mapping', 'FontSize', 6,...
            'Position', [10 100 100 50],'Callback', 'runner.mapping');       
            self.button5 = uicontrol('Style', 'togglebutton', 'String', 'Localization', 'FontSize', 6,...
            'Position', [10 10 100 50],'Callback', 'runner.localization');       
%            self.button6 = uicontrol('Style', 'checkbox', 'value', 1, 'String', 'visualize', 'Position', [400 20 70 50]);       
            %add axis
%            ax1 = axes('Position',[0.1 0.4 0.5 0.5]);

            drawnow;            
            
        end
        
        
        function loadSensorData(self,file_index,anchor)
            
            disp(['loading the sensors given in file ' self.sensor_param_json]);
            %we want to do this once since it is difficult to "unbind"
            %sockets in JeroMQ.
            
            self.sensor_param = i_o.readJsonFile(self.sensor_param_json);
            
            self.data_param = i_o.readJsonFile(self.data_param_json);
                        
            visulizer_adress = ['tcp://' self.data_param.ZMQ_send.ip_adress ':' ...
                    num2str(self.data_param.ZMQ_send.port)];
            
            self.gridmapsender = api.GridMapSender(self.jar_file,visulizer_adress);

            %if we have simulator
            if strcmp(self.data_param.source,'simulator')
                signal     = slam.simulator.Scenario(self.data_param,self.sensor_param);
                self.sensorQueue = signal.getSensorQueue();
            else
                %add sensor objects
                for i = 1:length(self.sensor_param.sensors)
                    
                    if strcmp(self.sensor_param.sensors{i}.sensor_type,'radar_P2')
                        sensors{i} = slam.sensor.RadarSensor(self.sensor_param.sensors{i});
                    elseif strcmp(self.sensor_param.sensors{i}.sensor_type,'radar_R1')
                        sensors{i} = slam.sensor.RadarSensor(self.sensor_param.sensors{i});
                    elseif strcmp(self.sensor_param.sensors{i}.sensor_type,'lidar')
                        sensors{i} = slam.sensor.LidarSensor(self.sensor_param.sensors{i});
                    elseif strcmp(self.sensor_param.sensors{i}.sensor_type,'ego')
                        sensors{i} = slam.sensor.EgoSensor(self.sensor_param.sensors{i});
                    else
                        error('uknown sensor type in setup file');
                    end
                    
                end
                %add sensor source

                if strcmp(self.data_param.source,'sensor')  %live data
                    
                    for i = 1:length(sensors)
                        if strcmp(sensors{i}.param.sensor_type,'radar_P2')
                            
                            adress      = strcat('tcp://',self.data_param.ZMQ_receive(i).ip_adress,':',num2str(self.data_param.ZMQ_receive(i).port));
                            jar_file    = getenv('JAR_FILE');
                            radar_reader = api.RadarReader(jar_file,adress);
                            sensors{i}.addSource(radar_reader);
                            
                        elseif strcmp(sensors{i}.param.sensor_type,'radar_R1')
                            
                            adress      = strcat('tcp://',self.data_param.ZMQ_receive(i).ip_adress,':',num2str(self.data_param.ZMQ_receive(i).port));
                            jar_file    = getenv('JAR_FILE');
                            radar_reader = api.RadarReader(jar_file,adress);
                            sensors{i}.addSource(radar_reader);
                            
                        elseif strcmp(sensors{i}.param.sensor_type,'lidar')
                            

                            [~, ~, LidarParamLf, LidarParamRf, ~, ~, ~, TrkParam, ~] = i_o.TrackingParam;
%                            [~, ~, LidarParamLf, LidarParamRf, ~, ~, ~, ~, ~] = i_o.TrackingParam;
                            [~, SS, ~] = i_o.GlobalParameters;
                            if strcmp(sensors{i}.param.description,'left-looking')
                                LidarParam = LidarParamLf;
                            elseif strcmp(sensors{i}.param.description,'right-looking')
                                LidarParam = LidarParamRf;
                            else
                                error('left or right?');
                            end
                            
                            lidar_reader = api.LidarReader(self.data_param.ZMQ_receive(i).ip_adress,...
                                self.data_param.ZMQ_receive(i).port,SS,LidarParam,TrkParam);
                            sensors{i}.addSource(lidar_reader);

                        elseif strcmp(sensors{i}.param.sensor_type,'ego')
                            
                            adress      = strcat('tcp://',self.data_param.ZMQ_receive(i).ip_adress,':',num2str(self.data_param.ZMQ_receive(i).port));
                            jar_file    = getenv('JAR_FILE');
                            
                            lon = 1.989515514159169;
                            lat = 48.758805697229462;
                            alt = 168;
%                             lat = 57.772761881773071;
%                             lon = 11.875549668224661;
%                             alt = 0;
                            ego_reader  = api.EgoReader(jar_file,adress,0,lon,lat,alt);
                            sensors{i}.addSource(ego_reader);
                            % if poles
                            if isfield(self.data_param,'poles_file')
                                poles_file   = fullfile(self.data_param.poles_file);
                                poles_p = load(poles_file);
                                self.poles_xyz = ego_reader.getPositionFromPole(poles_p.pole);
                            else
                                self.poles_xyz = [];
                            end
                            
                        else
                            error('uknown sensor type in setup file');
                        end
                    end

                elseif strcmp(self.data_param.source,'file') %recorded data
                
                    for i = 1:length(sensors)
                        if strcmp(sensors{i}.param.sensor_type,'radar_P2')
                            P2_file      = fullfile(self.data_param.P2_data.folder,self.data_param.P2_data.sub_folders{1},self.data_param.P2_data.file_names{file_index});
                            P2_reader    = api.RadarFileReaderNew(P2_file,self.data_param.P2_data.UTC_time_offset(file_index));
                            sensors{i}.addSource(P2_reader);
                        elseif strcmp(sensors{i}.param.sensor_type,'radar_R1')
                            R1_file      = fullfile(self.data_param.R1_data.folder,self.data_param.R1_data.sub_folders{1},self.data_param.R1_data.file_names{file_index});
                            R1_reader    = api.RadarFileReaderNewR1(R1_file,self.data_param.R1_data.UTC_time_offset(file_index));
                            sensors{i}.addSource(R1_reader);
                        elseif strcmp(sensors{i}.param.sensor_type,'lidar')
                            lidar_file   = fullfile(self.data_param.lidar_data.folder,self.data_param.lidar_data.sub_folders{1},self.data_param.lidar_data.file_names{file_index});
                            lidar_reader = api.LidarFileReaderRaw(lidar_file,self.data_param.lidar_data.UTC_time_offset(file_index));
                            sensors{i}.addSource(lidar_reader);
                        elseif strcmp(sensors{i}.param.sensor_type,'ego')
                            reference_file  = fullfile(self.data_param.reference_data.folder,self.data_param.reference_data.sub_folders{1},self.data_param.reference_data.file_names{file_index});
                            if nargin==3
                                ego_reader = api.EgoFileReader(reference_file, self.data_param.reference_data.angle_bias, anchor(1),anchor(2));
                            else
                                ego_reader = api.EgoFileReader(reference_file, self.data_param.reference_data.angle_bias);
                            end
                            sensors{i}.addSource(ego_reader);
                            %add the poles
                            poles_file   = fullfile(self.data_param.target_data.folder,self.data_param.target_data.sub_folders{1},self.data_param.target_data.file_names{1});
                            self.poles   = slam.util.MetalPoles(poles_file,ego_reader);

                        else
                            error('uknown sensor type in setup file');
                        end
                    end
                    
                else
                    error('uknown sensor source in setup file');
                end
                %setup the sensor queue
                self.sensorQueue   = slam.sensor.SensorQueue(sensors);
            end
        end
        
        function initFusion(self)
            
            disp(['creating the fusion engine based on parameters given in file ' self.slam_param_json]);

            %init the Fusion with slam param (and not sensor param since we
            %may change thoose along the way)
            self.slam_param = i_o.readJsonFile(self.slam_param_json);
            
            %we also need the limits the recorded sequence in order to set
            %up the grid space
            param = i_o.readJsonFile(self.data_param_json);
            self.slam_param.occupancy_grid.limits = param.limits(:)';
            
            self.fusion_engine = i_o.Fusion(self.slam_param);
            
        end
        
        function update_pf_params(self)
            self.matlab_version = self.slam_param.particle_filter.matlab_version;
            self.slam_param = i_o.readJsonFile(self.slam_param_json);
            self.fusion_engine.resetPF(self.slam_param);
        end
        
        function setupSensorsFusion(self)
            
            disp(['adding sensors to fusion egine']);

            %this function is used to push sensor settins to the Fusion
            %engine
            self.fusion_engine.setupSensors(self.sensorQueue.sensors,self.sensor_param,self.gridmapsender)
        end
        
        function b = measurements(self)
            
            iter = 1;
            
            while self.sensorQueue.hasNext()
                
                meas = self.sensorQueue.getNextMeasurement();
                if ~isempty(meas)
                    seconds = num2str(floor(meas.time),'%10.0f');
                    milliseconds = num2str(meas.time - floor(meas.time),'%0.8f');
                    
                    self.console_text1{meas.sensor_id}(1:length(seconds)) = seconds;
                    self.console_text2{meas.sensor_id}(1:length(milliseconds)) = milliseconds;
                end
                set(self.console_text_handle1,'String',cell2mat(self.console_text1));
                set(self.console_text_handle2,'String',cell2mat(self.console_text2));
                drawnow;
               
                if isa(meas,'slam.localization.EgoState')
                    self.fusion_engine.egoProvider.update(meas);
                end
                
                if ~self.fusion_engine.egoProvider.isempty() & ~meas.isempty()
                    
                    if isa(meas,'slam.sensor.RadarMeasurement') | isa(meas,'slam.sensor.LidarMeasurement')
                        ego_state = self.fusion_engine.egoProvider.get();
                        landmarks = self.sensorQueue.sensors{meas.sensor_id}.inverseMeasurementFunction(meas,ego_state);
                        self.gridmapsender.send_measurements(landmarks,self.fusion_engine.egoProvider.ego);
                        disp(['Sending measurements of size ' num2str(size(landmarks,2)) ' from sensor ' num2str(meas.sensor_id) ' , iteration ' num2str(iter)]);
                        iter = iter +1;
                    end
                end
            end
        end
                
        
        function mapping(self,matlab_visualizer)
            
            if nargin==1
                matlab_visualizer=false;
            end
            
            if matlab_visualizer
                figure(10);
                self.visualizer = slam.util.MappingVisualizer(self.fusion_engine.map,self.sensorQueue.sensors,self.poles);
   %            mapping_visualizer.addPoles(self.poles);
            end
            
            if ~isempty(self.poles_xyz)
                self.gridmapsender.send_measurements(self.poles_xyz);
            end
            
            iter = 1;
            
            while self.sensorQueue.hasNext() && get(self.button4,'value')==1
                
                meas = self.sensorQueue.getNextMeasurement();
                
                %visualize the times
                if ~isempty(meas)
                    if meas.sensor_id==6
                        meas = self.sensorQueue.sensors{6}.getNextMeasurement();
                    end
                    seconds = num2str(floor(meas.time),'%10.0f');
                    milliseconds = num2str(meas.time - floor(meas.time),'%0.8f');
                    
                    self.console_text1{meas.sensor_id}(1:length(seconds)) = seconds;
                    self.console_text2{meas.sensor_id}(1:length(milliseconds)) = milliseconds;
                end
                set(self.console_text_handle1,'String',cell2mat(self.console_text1));
                set(self.console_text_handle2,'String',cell2mat(self.console_text2));
                drawnow;
                 
                 %   if ~isempty(self.fusion_engine.egoProvider.ego)
                 %   self.sensorQueue.sensors{1}.plotFoV(self.fusion_engine.egoProvider.ego,50);
                 %   end
                 
                 self.fusion_engine.buffer(meas) % add to buffer
                 drawnow;
                if self.fusion_engine.mapping() % one map iteration
                    disp(['Mapping iteration: ' num2str(iter)]);
                    iter = iter +1;
                end
                if matlab_visualizer
                    self.visualizer.update(meas,self.fusion_engine.current_ego_state);
                end
                
            end
        end
        
        %----------- LOCALIZATION -----------
        
        function localization(self,matlab_visualizer)
            
            self.sensorQueue.reset(); % should only affect simulator
            
            self.update_pf_params(); % reloads pf params
            
            if nargin==1
                matlab_visualizer = false;
            end
            
                        
            if isa(self.fusion_engine.map,'slam.grid.TheGrid')
                self.GRID = self.fusion_engine.map;
                if self.matlab_version
                    self.fusion_engine.convertGrid();
                end
            end
            
            if matlab_visualizer
                self.visualizer = slam.util.Visualizer(self.sensorQueue.sensors);
                self.visualizer.initGrid(self.GRID);
            end
      
            self.gridmapsender.send_ogm(self.GRID,1);
            pause(0.1); %fucking fuck!!!
            if ~isempty(self.poles_xyz)
                self.gridmapsender.send_measurements(self.poles_xyz);
                pause(0.1); %fucking fuck!!
            end

            self.fusion_engine.reset();

            self.fusion_engine.egoProvider.particle_filter_init = false;
            if matlab_visualizer
                self.fusion_engine.initParticleFilter(self.visualizer);
            else
                self.fusion_engine.initParticleFilter();
            end             
            self.fusion_engine.delta_T = 0;
            
            iter = 1;

            while self.sensorQueue.hasNext() && get(self.button5,'value')==1
                
%                 if self.fusion_engine.delta_T > 2
%                     self.fusion_engine.initParticleFilter();
%                     self.fusion_engine.delta_T = 0;
%                 end
                    
                meas = self.sensorQueue.getNextMeasurement();
                
                %                if self.fusion_engine.initParticleFilter(self.visualizer)
                if 1
                    self.fusion_engine.buffer(meas) % add to buffer
                    if self.fusion_engine.filter()
                        
                        drawnow;
                        time = self.fusion_engine.particleFilter.last_update_time;
                        
                        if isa(self.sensorQueue.sensors{self.fusion_engine.egoProvider.ego_index}.sensorData,'slam.simulator.ScenarioDataGenerator')
                            ground_truth = self.sensorQueue.sensors{self.fusion_engine.egoProvider.ego_index}.sensorData.getState(time);
                        else
                            ground_truth = self.sensorQueue.sensors{self.fusion_engine.egoProvider.ego_index}.getNextMeasurement();
                        end
                        
                        self.fusion_engine.logger.putReference([ground_truth.pos;ground_truth.angle]);
                        self.fusion_engine.logger.putTime(time);
                        self.fusion_engine.logger.putData([self.fusion_engine.particleFilter.getEstimate().pos;self.fusion_engine.particleFilter.getEstimate().angle]);
                %        self.fusion_engine.logger.putParticles(self.fusion_engine.particleFilter);
                        
                        if matlab_visualizer
                            self.visualizer.update(self.fusion_engine.logger,self.fusion_engine.particleFilter);
                        end
                        
                        disp(['Filtering iteration: ' num2str(iter)]);
                        iter = iter + 1;
                        
                    end
                end
                
            end
 
        end
        
        function initVisualizer(self,GRID)
            
            self.visualizer = slam.util.Visualizer(self.sensorQueue.sensors);
            self.visualizer.initGrid(GRID);
            self.visualizer.addPoles(self.poles);
            self.visualizer.addTrajectory(self.sensorQueue.sensors{self.ego_index}.sensorData.getTrajectory());

        end
        
        function anchor = getAnchor(self)
        
            anchor = [self.sensorQueue.sensors{self.ego_index}.sensorData.lon_anchor,...
                      self.sensorQueue.sensors{self.ego_index}.sensorData.lat_anchor];
            
        end
        
        
        function initSensorConfiguration(self,subset_str)
            
            %form the structs for the sensor configuration
            %this should be done befonre loading data
            self.sensor_param = i_o.readJsonFile(self.sensor_param_json);
            %assume ego is last
            self.ego_index = length(self.sensor_param.sensors);
            
            %for real data we might need to adapt to some special use cases
            if strcmp(subset_str,'P2_only')
                %radar only:
                self.sensor_param.sensors = self.sensor_param.sensors([1,4]);
                self.sensor_param.measurement_grids = self.sensor_param.measurement_grids(1);
                self.ego_index = 2;
            elseif strcmp(subset_str,'R1_only')
                %lidar only
                self.sensor_param.sensors = self.sensor_param.sensors([2,4]);
                self.sensor_param.measurement_grids = self.sensor_param.measurement_grids(2);
                self.sensor_param.sensors{1}.sensor_id = 1;
                self.sensor_param.measurement_grids{1}.grid_index = 1;
                self.ego_index = 2;
            elseif strcmp(subset_str,'P2_R1')
                %lidar only
                self.sensor_param.sensors = self.sensor_param.sensors([1,2,4]);
                self.sensor_param.measurement_grids = self.sensor_param.measurement_grids(1:2);
                self.ego_index = 3;
            elseif strcmp(subset_str,'lidar_only')
                %lidar only
                self.sensor_param.sensors = self.sensor_param.sensors([3,4]);
                self.sensor_param.measurement_grids = self.sensor_param.measurement_grids(3);
                self.sensor_param.sensors{1}.sensor_id = 1;
                self.sensor_param.measurement_grids{1}.grid_index = 1;
                self.ego_index = 2;
            end
            
        end
        
    end
end

