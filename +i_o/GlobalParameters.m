function [param, SS, ObjectClass] = GlobalParameters
% System
% Use ego or ref data. ego=1, ref=2, ESP=3.
param.sourceEgo = uint8(1);

% Rest lidar time stamp to UTC time
param.resetLidarTime = true;

% If skip ego data setting, then use 0-vector as ego data.
param.skipEgoData = true;

% Sampling times for system.
param.Ts_System = 0.01;

% Interfaces
param.Ts_UDPRx    = 0.01;
param.Ts_UDPTx    = 0.01;
param.Ts_HSCANRx  = 0.005;
param.Ts_RefCANTx = 0.05;
param.Ts_RadarTx  = 0.04;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Invalid and error values for signals %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Unsigned int 8 %
SS.u1_Invalid = uint8(255);
SS.u1_Error   = uint8(254);

% Signed int8 %
SS.s1_Invalid = int8(127);
SS.s1_Error   = int8(126);

% Unsigned int 16 %
SS.u2_Invalid = uint16(65535);
SS.u2_Error   = uint16(65534);

% Float 32 %
SS.fl_Invalid     = single(1001);
SS.fl_Error       = single(1002);
SS.fl_Std_Invalid = single(-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Object Class definitions  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Motor vehicles %
ObjectClass.MotorVehicle = 10;
ObjectClass.PassengerCar = 11; 
ObjectClass.Truck        = 12;
ObjectClass.Bus          = 14;
ObjectClass.Motorcycle   = 15;
ObjectClass.Train        = 16;

% Vulnarable road users (VRU) %
ObjectClass.VRU        = 20;
ObjectClass.Pedestrian = 21;
ObjectClass.Cyclist    = 22;
ObjectClass.Animal     = 23;

% Other on road objects %
ObjectClass.OtherObj       = 30;
ObjectClass.OtherSafe      = 31;
ObjectClass.OtherDangerous = 32;

% Unknown Objects %
ObjectClass.Unknown          = 90;
ObjectClass.UnknownSmallObj  = 91;
ObjectClass.UnknownMediumObj = 92;
ObjectClass.UnknownLargeObj  = 93;
