classdef EgoProvider<i_o.Buffer

    properties
        ego
        particle_filter_init
        ego_index
        
    end
    
    methods
        
        function self = EgoProvider(ego_index)
                        
            %EgoProvider contains both a Buffer and an estimate
            self@i_o.Buffer(100);
            self.particle_filter_init = false;
            self.ego_index = ego_index;

        end
        
        function update(self,meas)
                        
            self.update@i_o.Buffer(meas);
%             disp(['ego buffer size ' num2str(self.index)]);
            
            if ~meas.isempty()
                self.ego = meas;
            end
        end
        
        function init(self)
            self.particle_filter_init = true;
        end
                
        function time = getTime(self)
            
            time = [];
            for i=1:self.index
                time = [time,self.measurements{i}.time];
            end        
        end
        
        function ego = get(self)
           
            ego = self.measurements{1};
        
        end
                        
        function reset(self)
            self.particle_filter_init = false;
        end 
        
    end
end

