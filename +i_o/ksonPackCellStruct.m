function mout = ksonPackCellStruct(cin)
%#codegen
% Concatinate input cell.
c        = cin(:);
% Get number of elements (structs) in cell.
Nstructs = length(c(:));
% Set pre-deifined size of message m. Currently this needs to guessed, so set
% it large to be sure that total message can be packed in m.
m    = zeros(1,100000,'uint8'); 
% Set first byte to contain the number of structs.
m(1) = typecast(uint8(Nstructs),'uint8');
% Set index used for writing struct messages mii in m.
idx  = 1;
for ii=1:Nstructs
    % Get struct number ii.
    s                 = c{ii};
    % kson encode struct ii.
    mii               = i_o.ksonEncode(s,ii);
    % Get length of message mii.
    Nmii              = length(mii);
    % Write message mii to m.
    m(idx+1:idx+Nmii) = mii;
    % Increase counter for writing data to m.
    idx               = idx + Nmii;
end
% Pick out the final message.
mout = m(1:idx);

    