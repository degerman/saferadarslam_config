function [x,y,z,covCart] = spher2cart(range,azim,elev,covSpher,SS)
% Function computes the cartesian cordinates x,y,z and its covariance
% matrix, given the sperical coordinates range,azim,elev and it covariance
% matrix. 

%#codegen

% Max number of detections
Ndet    = numel(range);

% Initiate output data
x       = SS.fl_Invalid*ones(Ndet,1,'single');
y       = SS.fl_Invalid*ones(Ndet,1,'single');
z       = SS.fl_Invalid*ones(Ndet,1,'single');
covCart = SS.fl_Invalid*ones(3,3,Ndet,'single');

% Set output data for all valid detections
val = find(range~=SS.fl_Invalid);
for i = 1:length(val)
    % Compute x,y,z.
    x(val(i)) = range(val(i))*cos(elev(val(i)))*cos(azim(val(i)));
    y(val(i)) = range(val(i))*cos(elev(val(i)))*sin(azim(val(i)));
    z(val(i)) = range(val(i))*sin(elev(val(i)));
    % Compute partial derivatives of x,y,z.
    dx  = [cos(elev(val(i)))*cos(azim(val(i))), ...
           -range(val(i))*cos(elev(val(i)))*sin(azim(val(i))), ...
           -range(val(i))*sin(elev(val(i)))*cos(azim(val(i)))];
    dy  = [cos(elev(val(i)))*sin(azim(val(i))), ...
           range(val(i))*cos(elev(val(i)))*cos(azim(val(i))), ...
           -range(val(i))*sin(elev(val(i)))*sin(azim(val(i)))];
    dz  = [sin(elev(val(i))) 0 range(val(i))*cos(elev(val(i)))];
    % Compose gradient of x,y,z.
    gradDet = [dx; dy; dz];
    % Compute covariance matrix of x,y,z.
    if size(covSpher,3)==1
        covCart(:,:,val(i)) = gradDet*covSpher*gradDet';
    else
        covCart(:,:,val(i)) = gradDet*covSpher(:,:,val(i))*gradDet';
    end
end