function LidarData_frame = struct_LidarData(SensParam,SS)
%#codegen
Ndet = SensParam.Ndet;

LidarData_frame.fl_CurrTime = SS.fl_Invalid;
LidarData_frame.au2_Range   = SS.u2_Invalid*ones(Ndet,1,'uint16');
LidarData_frame.au1_SNR     = SS.u1_Invalid*ones(Ndet,1,'uint8');