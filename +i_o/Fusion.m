classdef Fusion<handle
    
    properties
        
        logger
        nrParticles
        weight_scaling
        
        sensors
        radars
        lidars
        gridmapsender
        
        egoProvider
        current_ego_state
        radarBuffer
        lidarBuffer
        
        radarGrid
        lidarGrid
        map
        
        motionModel
        particleFilter
        
        MATLAB
        
        dt
        verbose
        
        ego
        
        delta_T
        
        matlab_version % pf matlab version
    end
    
    methods
        
        %--------- SETUP FUNCTIONS ---------
        
        function self = Fusion(slam_param,verbose)
            
            self.verbose = slam.util.Verbose();
            if( nargin>2 )
                if( verbose )
                    self.verbose.enable();
                else
                    self.verbose.disable();
                end
            end
            
            %setup the map, only done once
            if strcmp(slam_param.map_type,'Gibbs')
                self.map    = slam.gibbs_iterative.Gibbs(slam_param.gibbs);
            else
                self.map    = slam.grid.TheGrid(slam_param.occupancy_grid);
            end
            
            %setup some basiv particle filter parameters
            self.resetPF(slam_param)
            self.logger     = i_o.DataLogger();
            
        end
        
        function resetPF(self, slam_param)
            self.nrParticles = slam_param.particle_filter.particles;
            self.weight_scaling = slam_param.particle_filter.weight_scaling;
            self.motionModel = slam.localization.FullMotionModel(slam_param.particle_filter);
            self.matlab_version = slam_param.particle_filter.matlab_version;
        end
        
        function setupSensors(self,sensors,sensor_param,gridmapsender)
            
            self.sensors = sensors;
            
            %set up sensor grids
            for i=1:length(self.sensors)
                if isa(sensors{i},'slam.sensor.RadarSensor')
                    self.radarGrid{self.sensors{i}.param.sensor_id}  = ...
                        slam.grid.RadarGrid(self.sensors{i},sensor_param.measurement_grids{i});
                end
                if isa(sensors{i},'slam.sensor.LidarSensor')
                    self.lidarGrid{self.sensors{i}.param.sensor_id}  = ...
                        slam.grid.LidarGrid(self.sensors{i},sensor_param.measurement_grids{i});
                end
                if isa(sensors{i},'slam.sensor.EgoSensor')
                    self.egoProvider = i_o.EgoProvider(self.sensors{i}.param.sensor_id);
                end
            end
            self.radarBuffer = i_o.Buffer(5);
            self.lidarBuffer = i_o.Buffer(5);
            self.gridmapsender = gridmapsender;
            
        end
        
        %--------- PROCESSING FUNCTIONS ---------
        
        function buffer(self,meas)
            if isa(meas,'slam.localization.EgoState')
                %    self.egoProvider.update(meas);
            elseif isa(meas,'slam.sensor.RadarMeasurement')
                self.radarBuffer.update(meas);
            elseif isa(meas,'slam.sensor.LidarMeasurement')
                self.lidarBuffer.update(meas);
            end
        end
        
        function b = mapping(self)
            b1 = self.mapping_buffer(self.radarBuffer,self.radarGrid);
            b2 = self.mapping_buffer(self.lidarBuffer,self.lidarGrid);
            b = b1 | b2;
        end
        
        function b = mapping_buffer(self,sensorBuffer,sensorGrid)
            
            b = false;
            
            ego_state = self.sensors{self.egoProvider.ego_index}.getNextMeasurement();
            self.egoProvider.update(ego_state);

            self.gridmapsender.send_ego(self.egoProvider.ego);
            
            while sensorBuffer.hasNext()
                
                disp(['buffer length ' num2str(sensorBuffer.index)]);
                meas   = sensorBuffer.getNext();
                drawnow;
                
                                
                if isa(meas,'slam.sensor.RadarMeasurement')
                    %                     meas = self.sensors{meas.sensor_id}.selectStationary(meas,ego_state,self.verbose);
                    idx = (meas.doppler >= -ego_state.speed) & (meas.doppler < 0);
                    meas.range = meas.range(idx);
                    meas.SNR = meas.SNR(idx);
                    meas.u = meas.u(idx);
                    meas.v = meas.v(idx);
                    meas.doppler = meas.doppler(idx);
                    if isempty(meas.range)
                        meas.empty = true;
                    end
                    %NOTE!!!!!!!!!!!!! Radar tends to be one seconds off
                    meas.time = meas.time + 1;

                end
                
                if isa(meas,'slam.sensor.LidarMeasurement')
                    
                    if abs(ego_state.speed)<0.1 | abs(ego_state.angle_rate(1)) > 2
                         meas.empty = true;
                    end
                end
                
                if isa(self.sensors{self.egoProvider.ego_index}.sensorData,'slam.simulator.ScenarioDataGenerator')
                    self.current_ego_state = self.sensors{self.egoProvider.ego_index}.sensorData.getState(meas.time);
                else
                    self.current_ego_state = self.egoProvider.peekSync(meas.time);
                end
                                
                if ~meas.isempty
                    
                    b = true;
%                    drawnow;      
                    %measurement is accepted
                    T       = meas.time - self.current_ego_state.time;
                    self.dt = [self.dt,T];
                    self.current_ego_state.predict(T);
                    drawnow;
                    self.verbose.print(['Mapping, sensor ' num2str(meas.sensor_id) ': meas time - ego time = ' num2str(T) ' buffer size ' num2str(self.radarBuffer.index)]);
                    
                    if isa(self.map,'slam.gibbs_iterative.Gibbs')
                        self.map.update(meas,self.current_ego_state,self.sensors{meas.sensor_id})
                    else
                        sensorGrid{meas.sensor_id}.update(meas);
                        %                        bounding_box = sensorGrid{meas.sensor_id}.getBoundingBox(ego_state);
                        self.map.regionOfInterest.update(self.current_ego_state,50);
                        self.map.update(sensorGrid{meas.sensor_id},self.current_ego_state);
                        self.gridmapsender.send_ogm(self.map);
                        
                    end
                else
                    self.verbose.print('empty measurement');
                end
            end
        end
        
        
        function convertGrid(self)
            self.map = self.map.convert2C();
        end
        
        function b = initParticleFilter(self,visualizer)
            
            %see if we have initiated the particle filter
            b = false;
            
            if self.egoProvider.particle_filter_init
                b = true;
            else
                if isa(self.sensors{self.egoProvider.ego_index}.sensorData,'slam.simulator.ScenarioDataGenerator')
                    self.currenty_ego_state = self.sensors{self.egoProvider.ego_index}.sensorData.getState(0);
                    self.current_ego_state.time = 0;
                else
                    self.current_ego_state = self.sensors{self.egoProvider.ego_index}.getNextMeasurement();
                end
                
                %                self.egoProvider.update(ego_state);
                %                drawnow;
                
                if self.matlab_version
                    %MATLAB version
                    self.particleFilter = slam.localization.ParticleFilter(self.weight_scaling, self.nrParticles, self.map);
                    self.particleFilter.init(self.current_ego_state,self.motionModel,self.sensors);
                else
                    %C++ version
                    self.particleFilter = slam.localization.cfunc.particleFilter(self.nrParticles, self.map, self.sensors, 0.1);
                    self.particleFilter.init(self.current_ego_state, self.motionModel, self.sensors);
                end
                
                %                self.egoProvider.update(ego_state);
                self.particleFilter.last_update_time = self.current_ego_state.time;
                
                self.egoProvider.init();
                if nargin==2 && ~isempty(visualizer)
                    visualizer.initPF(self.particleFilter);
                end
                b = true;
                self.verbose.print('initiating filter');
            end
        end
        
        function b = filter(self)
            b1 = self.filter_buffer(self.radarBuffer);
            b2 = self.filter_buffer(self.lidarBuffer);
            b = b1 | b2;
        end
        
        function b = filter_buffer(self,sensorBuffer)
            
            b = false;
            
            %                     if isa(self.sensors{self.egoProvider.ego_index}.sensorData,'slam.simulator.ScenarioDataGenerator')
            %                         ego_state = self.sensors{self.egoProvider.ego_index}.sensorData.getState(meas.time);
            %                     else
            ego_state = self.sensors{self.egoProvider.ego_index}.getNextMeasurement();
            self.egoProvider.update(ego_state);
            drawnow;
            %             self.gridmapsender.send_ego(self.egoProvider.ego); % plots
            %             the most recent ego when there are no radar meas
            
            while sensorBuffer.hasNext() && ~self.egoProvider.isempty()
                
                meas   = sensorBuffer.getNext();
                
                
                if isa(meas,'slam.sensor.RadarMeasurement')
                    %                     meas = self.sensors{meas.sensor_id}.selectStationary(meas,ego_state,self.verbose);
                    idx = (meas.doppler >= -self.current_ego_state.speed) & (meas.doppler < 0);
                    meas.range = meas.range(idx);
                    meas.SNR = meas.SNR(idx);
                    meas.u = meas.u(idx);
                    meas.v = meas.v(idx);
                    meas.doppler = meas.doppler(idx);
                    if isempty(meas.range)
                        meas.empty = true;
                    end
                    %NOTE!!!!!!!!!!!!! Radar tends to be one seconds off
                    meas.time = meas.time + 1;

                end
                
                if isa(meas,'slam.sensor.LidarMeasurement')

                    meas.idx  = meas.range<50 & abs(meas.azimuth)<0.78;  %45 degrees
                    meas.range = meas.range(meas.idx);
                    meas.azimuth = meas.azimuth(meas.idx);
                    meas.elevation = meas.elevation(meas.idx);
                    meas.SNR = meas.SNR(meas.idx);
                    meas.u = meas.u(meas.idx);
                    meas.v = meas.v(meas.idx);
                    
                    if abs(ego_state.speed)<0.1
                        meas.empty = true;
                    end
                end
                
                if isa(self.sensors{self.egoProvider.ego_index}.sensorData,'slam.simulator.ScenarioDataGenerator')
                    self.current_ego_state = self.sensors{self.egoProvider.ego_index}.sensorData.getState(meas.time);
                    self.current_ego_state.time = meas.time;
                else
                    self.current_ego_state = self.egoProvider.peekSync(meas.time);
                    self.current_ego_state.predict(meas.time - self.current_ego_state.time);
                end

                if ~meas.isempty()
                    
                    b = true;
                                        
                    self.verbose.print(['Localization: meas time - ego time = ' num2str(meas.time-self.current_ego_state.time)]);
                    
                    self.delta_T = meas.time - self.particleFilter.last_update_time;
%                     if self.delta_T > 2
%                         disp('delta_T <2')
%                         break;
%                     end
                                        
                    if self.delta_T>0
                         
                        %               T = 0.1;
                        if self.matlab_version
                            
                            self.particleFilter.updateMeasurements({meas},self.current_ego_state);
                            self.particleFilter.particleTimeUpdate(self.delta_T);
                            self.particleFilter.kalmanTimeUpdate(self.delta_T);
                        else
                            %                         self.particleFilter.resample();
                            
                            self.particleFilter.updateMeasurements({meas},self.current_ego_state, self.delta_T);
                            self.particleFilter.particleTimeUpdate();
                            self.particleFilter.kalmanTimeUpdate();
                        end
                        
                        
                        self.particleFilter.evaluateWeights();
                        
                        self.particleFilter.stateEstimation();
                        
                        self.particleFilter.resample();
                        
                        self.particleFilter.kalmanMeasUpdate();
                        
%                         self.particleFilter.stateEstimation();

                        %                        self.particleFilter.setCurrentTime(meas.time)
                        self.particleFilter.last_update_time = meas.time;
                        
                        if norm(self.particleFilter.getParticleVariance()) > 3
                            self.particleFilter.resample_ground_truth();
                            
                            self.particleFilter.stateEstimation();
                            disp('Relocating due to variance!!!!!!!!!!!!!!!!!!!!!!!');
                        end
                        
                        self.verbose.print(['Localization update using delta T ' num2str(self.delta_T)]);
                        
                        state_estimate = self.particleFilter.getEstimate;
                        
                        self.gridmapsender.send_localization(self.particleFilter,self.current_ego_state);
                        landmarks = self.sensors{meas.sensor_id}.inverseMeasurementFunction(meas,state_estimate);
                        self.gridmapsender.send_measurements(landmarks);
                        %                     % plots the measured ego (not
                        %                     correct)
                        %                     self.gridmapsender.send_localization(self.particleFilter,self.egoProvider.ego); % plots most recent ego
                        if any(isnan(state_estimate.pos))
                            keyboard
                        end
                        sprintf('Error of: %f \n',state_estimate.pos - self.current_ego_state.pos)
                        %   sprintf('Time Error: %f \n',self.current_ego_state.time-self.current_ego_state.time)
                    else
                        self.verbose.print('negative t');
                    end
                else
                    disp('empty measurement');
                end
            end
            
        end
        
        function reset(self)
            self.logger.reset();
            self.radarBuffer.reset();
            self.lidarBuffer.reset();
            self.egoProvider.reset();
        end
        
        function plot_results(self)
            self.logger.plotErrorLongLat()
        end
        
        function error = get_results(self)
            error = self.logger.getErrorLongLat();
        end
        
    end
end
