function [x_c,y_c,z_c,covCart_c,ID_c,Layer_c,SNR_c,delay_c] = ...
    clusteringLidar(x,y,z,covCart,layer,snr,delay,LidarParam,SS,TrkParam)

%#codegen
TH_X     = 0.2; 
thresh_x = LidarParam.thresh_x; 
thresh_y = LidarParam.thresh_y;
m        = 2; 
% Init clustered data
x_c       = SS.fl_Invalid*ones(1,LidarParam.NdetLayer,'single');
y_c       = SS.fl_Invalid*ones(1,LidarParam.NdetLayer,'single');
z_c       = SS.fl_Invalid*ones(1,LidarParam.NdetLayer,'single');
varx_c    = SS.fl_Invalid*ones(1,LidarParam.NdetLayer,'single');
vary_c    = SS.fl_Invalid*ones(1,LidarParam.NdetLayer,'single');
varz_c    = SS.fl_Invalid*ones(1,LidarParam.NdetLayer,'single');
covCart_c = SS.fl_Invalid*ones(3,3,LidarParam.NdetLayer,'single');
SNR_c     = SS.u1_Invalid*ones(1,LidarParam.NdetLayer,'uint8');
delay_c   = 0*ones(1,LidarParam.NdetLayer,'single');
Layer_c   = SS.u1_Invalid*ones(1,LidarParam.NdetLayer,'uint8');
ID_c      = SS.u2_Invalid*ones(1,LidarParam.NdetLayer,'uint16');
% Reshape input data to lidar scanning scedual
c     = SS.u2_Invalid*ones(LidarParam.NdetLayer*LidarParam.NrOfLayers,1,'uint16');
c     = reshape(c,[LidarParam.NdetLayer LidarParam.NrOfLayers])';
X     = reshape(x,[LidarParam.NdetLayer LidarParam.NrOfLayers])';
Y     = reshape(y,[LidarParam.NdetLayer LidarParam.NrOfLayers])';
Z     = reshape(z,[LidarParam.NdetLayer LidarParam.NrOfLayers])';
varX  = reshape(squeeze(covCart(1,1,:)),[LidarParam.NdetLayer LidarParam.NrOfLayers])';
varY  = reshape(squeeze(covCart(2,2,:)),[LidarParam.NdetLayer LidarParam.NrOfLayers])';
varZ  = reshape(squeeze(covCart(3,3,:)),[LidarParam.NdetLayer LidarParam.NrOfLayers])';
SNR   = reshape(snr,[LidarParam.NdetLayer LidarParam.NrOfLayers])';
Layer = reshape(layer,[LidarParam.NdetLayer LidarParam.NrOfLayers])';
Delay = reshape(delay,[LidarParam.NdetLayer LidarParam.NrOfLayers])';

% Cluster data
[X,I] = sort(X,1,'ascend');
for k = 1:LidarParam.NdetLayer
    Y(:,k) = Y(I(:,k),k);
end
Iinv = I;
for k = 1:LidarParam.NdetLayer
    for ii = 1:LidarParam.NrOfLayers
        Iinv(I(ii,k),k) = ii;
    end
end
for k = 1:LidarParam.NdetLayer
    c(1,k) = k;
    for ii = 2:LidarParam.NrOfLayers
        if X(ii,k) < X(ii-1,k)+TH_X && X(ii,k) > X(ii-1,k)-TH_X
            c(ii,k) = c(ii-1,k);
        elseif X(ii,k) > 40 && X(ii,k) < X(ii-1,k)+TH_X*3 && X(ii,k) > X(ii-1,k)-TH_X*3
            c(ii,k) = c(ii-1,k);
        else
            c(ii,k) = k+ii*LidarParam.NdetLayer;
        end
    end
end
c(X == SS.fl_Invalid) = SS.u2_Invalid;
for k = 1:LidarParam.NdetLayer
    A = uint16(1);
    F = uint16(1);
    ar = c(c(:,k) ~= SS.u2_Invalid,k);
    TH = 1;
    id = ar == SS.u2_Invalid;
    for v = 1:length(ar)
        if    sum(ar == ar(v)) > TH
            TH = sum(ar == ar(v));
            id = ar == ar(v);
        end
        if ~isempty(ar(id))
            B = uint16(ar(id));
            F = uint16(length(ar(id)));
            A = B(1);
        end
    end
    if F>m || mean(X(c(:,k) == A,k)) > 40 && F>(m)
        c(c(:,k) ~= A,k) = SS.u2_Invalid;
        mini = min(X(c(:,k) ~= SS.u2_Invalid,k));
        c(X(:,k) ~= mini,k) = SS.u2_Invalid;
    else
        c(:,k) = SS.u2_Invalid;
    end
    if sum(c(:,k) ~= SS.u2_Invalid) > 1
        ix = find(c(:,k) ~= SS.u2_Invalid);
        c(ix(ix ~= ix(1)),k) = SS.u2_Invalid;
    end
end
ctmp = reshape(c,[1 LidarParam.NdetLayer*LidarParam.NrOfLayers]);
a    = unique(ctmp','rows');
for k = 1:length(a(a < LidarParam.NdetLayer*LidarParam.NrOfLayers+1))
    c(c == a(k)) = k;
end
% set output data
x_c(any(c ~= SS.u2_Invalid))           = X(c ~= SS.u2_Invalid);
y_c(any(c ~= SS.u2_Invalid))           = Y(c ~= SS.u2_Invalid);
z_c(any(c ~= SS.u2_Invalid))           = Z(c ~= SS.u2_Invalid);
SNR_c(any(c ~= SS.u2_Invalid))         = SNR(c ~= SS.u2_Invalid);
Layer_c(any(c ~= SS.u2_Invalid))       = Layer(c ~= SS.u2_Invalid);
delay_c(any(c ~= SS.u2_Invalid))       = Delay(c ~= SS.u2_Invalid);
varx_c(any(c ~= SS.u2_Invalid))        = varX(c ~= SS.u2_Invalid);
vary_c(any(c ~= SS.u2_Invalid))        = varY(c ~= SS.u2_Invalid);
varz_c(any(c ~= SS.u2_Invalid))        = varZ(c ~= SS.u2_Invalid);
covCart_c(:,:,any(c ~= SS.u2_Invalid)) = 0;
covCart_c(1,1,:)                       = varx_c;
covCart_c(2,2,:)                       = vary_c;
covCart_c(3,3,:)                       = varz_c;
SNR_c                                  = SNR_c';
Layer_c                                = Layer_c';
delay_c                                = delay_c';

XX = x_c(x_c < SS.fl_Invalid)';
YY = y_c(y_c < SS.fl_Invalid)';

Ctmp = ones(1,size(XX,1),'uint16');
idx = 1:LidarParam.NdetLayer;

idx = idx(x_c < SS.fl_Invalid);
for ii = 1:length(XX)-1
    if XX(ii) < (XX(ii+1)+thresh_x)...
            && XX(ii) > (XX(ii+1)-thresh_x)...
            && YY(ii) < (YY(ii+1)+thresh_y)...
            && YY(ii) > (YY(ii+1)-thresh_y)
        Ctmp(ii+1) = Ctmp(ii);
    else
        Ctmp(ii+1) = Ctmp(ii)+1;
    end
end
ID_c(idx) = Ctmp + TrkParam.Ntrk;
% simulscript(x_c,y_c,x,y,endt,DetData.au2_TrkID)