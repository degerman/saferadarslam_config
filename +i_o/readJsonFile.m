function param = readJsonFile(file)

%read parameters
txt         = fileread(file);
param       = jsondecode(txt);
            
%find the angles and convert to radians

param = deg2radStruct(param);

%check sub-fields (which can be

fields = fieldnames(param);


for index = 1:numel(fields)
    
    field = fields{index};
    
    if iscell(param.(field)) %if cell
        
        n = length(param.(field)); %possibly array
        
        for i = 1:n
            
            param.(field){i} = deg2radStruct(param.(field){i});
            
        end
        
    else
        
        if isstruct(param.(field)) % if struct
            
            n = length(param.(field)); %possibly array
            
            for i = 1:n
                
                param.(field)(i) = deg2radStruct(param.(field)(i));
                
                %subfields
                
                subfields = fieldnames(param.(field)(i));
                
                for subindex = 1:numel(subfields)
                    
                    subfield = subfields{subindex};
                    
                    if isstruct(param.(field)(i).(subfield)) % if substruct
            
                        n2 = length(param.(field)(i).(subfield)); %possibly array
            
                        for i2 = 1:n2
                
                            param.(field)(i).(subfield)(i2) = deg2radStruct(param.(field)(i).(subfield)(i2));
                    
                        end
                    end
                end
                
            end
        end
    end
end
end

function param = deg2radStruct(param)

if isstruct(param)
    
    if isfield(param,'measurement_grids') %make cell array if struct
        if isstruct(param.measurement_grids)
            tmp = {};
            for i = 1:length(param.measurement_grids)
                tmp{i} = param.measurement_grids(i);
            end
            param.measurement_grids = tmp;
        end
    end
    if isfield(param,'sensors') %make cell array if struct
        if isstruct(param.sensors)
            tmp = {};
            for i = 1:length(param.sensors)
                tmp{i} = param.sensors(i);
            end
            param.sensors = tmp;
        end
    end
    
    if isfield(param,'az_FoV')
        param.('az_FoV') = param.('az_FoV')*pi/180;
    end
    if isfield(param,'el_FoV')
        param.('el_FoV') = param.('el_FoV')*pi/180;
    end
    if isfield(param,'az_offset')
        param.('az_offset') = param.('az_offset')*pi/180;
    end
    if isfield(param,'el_offset')
        param.('el_offset') = param.('el_offset')*pi/180;
    end
    if isfield(param,'yaw')
        param.('yaw') = param.('yaw')*pi/180;
    end
    if isfield(param,'pitch')
        param.('pitch') = param.('pitch')*pi/180;
    end
    if isfield(param,'roll')
        param.('roll') = param.('roll')*pi/180;
    end
    if isfield(param,'yaw_rate')
        param.('yaw_rate') = param.('yaw_rate')*pi/180;
    end
    if isfield(param,'pitch_rate')
        param.('pitch_rate') = param.('pitch_rate')*pi/180;
    end
    if isfield(param,'roll_rate')
        param.('roll_rate') = param.('roll_rate')*pi/180;
    end
    if isfield(param,'angle_bias')
        param.('angle_bias') = param.('angle_bias')*pi/180;
    end
    
end
end
