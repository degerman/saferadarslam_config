classdef DataReader<handle
    %FileReader This loads data and pushes measurements to the system
    %   A first temporary FileReader is implmemented
    
    properties
        
        info
        
        %sensorData
                
        index
        N
        
        UTC_time_offset

    end
    
    methods
                       
        function [t_start,t_end] = getTimeInterval(self)
            
            t_start = [];
            t_end = [];
            
        end
        
        function reset(self)
            
            self.index = 1;
            
        end
        
        function bool = hasNext(self)
            
            bool = self.index<(self.N+1);
            
        end
        
    end
    
    
    methods(Static)
        
        
        
    end
    
end

