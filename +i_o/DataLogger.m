classdef DataLogger<handle
    %Data Logger 
    
    properties
        
        directory
        
        ego_data
        ego_ground_truth
        
        LM_ground_truth
        measurements
        
        landmarks
        
        particle_position
        particle_weight
        
        time
                
    end
    
    methods
                       
        function self = DataLogger(directory)
            
            if nargin == 1
                self.directory = directory;
            else
                self.directory = [];
            end
        end
        
        function reset(self)
           
            self.ego_data = [];
            self.ego_ground_truth = [];
            self.time = [];
        end
        
        function putData(self,ego_data)
            
            self.ego_data = [self.ego_data,ego_data];
            
        end
        
        function putMeasurement(self,meas)
            
            self.measurements = [self.measurements,meas];

        end
        
        function putParticles(self,particle_filter)
            for i=1:particle_filter.nrParticles
                [~,state,w] = particle_filter.getParticle(i);
                state = state.getNonLinear();
                self.particle_position(:,i)=state(1:3);
                self.particle_weight(i)=w;
            end
        end
        
        
        function putReference(self,ego_ground_truth)
            
            self.ego_ground_truth = [self.ego_ground_truth,ego_ground_truth];
        end
        
        function putTime(self,time)

            self.time = [self.time, time];
        end
            
        
        function plotError(self)
            
            error = self.getError();
            
            figure()
            plot(self.time,error(1:3,:)','-')
            legend('x','y','z')
            title('Position error')
            
            figure()
            plot(self.time,rad2deg(error(4:6,:))','-')
            legend('y','p','r')
            title('Angular error')
        end
        
        function plotErrorLongLat(self)
            error = self.getErrorLongLat();

            figure
            plot(self.time,error(1,:))
            hold on
            plot(self.time,error(2,:))
            plot(self.time,error(3,:))
            legend('longitudinal','lateral','alt')
            title('Error in long/lat/alt')
            xlabel('time step (s)')
            ylabel('error (m)')
        end

        function error = getError(self)
            error = self.ego_ground_truth - self.ego_data;
        end

        function error = getErrorLongLat(self)
            N = size(self.ego_data,2);
            error = zeros(3,N);
            for i=1:N
                R = slam.sensor.Sensor.rotationMatrix3D(self.ego_ground_truth(4,i),self.ego_ground_truth(5,i),self.ego_ground_truth(6,i))';
                %from ground to vehicle frame is the inverted
                invR = R';
                err = self.ego_ground_truth(1:3,i)-self.ego_data(1:3,i);
                error(:,i) = invR*err;
            end
        end
            
        
    end
    
    methods(Static)
        
        
        
    end
    
end

