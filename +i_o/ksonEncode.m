function m = ksonEncode(vin,ID)
%#codegen
% If input is numeric then make it to struct.
% If it is not struct and not numeric set it to 0.
if ~isstruct(vin) && isnumeric(vin)
    v.field1 = vin;
elseif isstruct(vin)
    v = vin;
else
    v.felt1 = uint8(0);
end
% Get cell array of field names in the struct.
feltNames = fieldnames(v);
% Get char array of all characters in the field names in order.
chars = [feltNames{:}];

% Get total number of characters.
Nchars       = length(chars);

% Get total number of field names.
Nfelt       = length(feltNames);
% Set number of bytes to represent the total number of fields.
Nfelt_bytes = 2;

% Get vector of the field names character-length.
feltNamesLength       = zeros(1,Nfelt);
% Set number of bytes to represent the total number of character-lengths.
feltNamesLength_bytes = 2;

% Init vector for the number of bytes of the fields's data-type.
feltTypes       = ones(1,Nfelt);
% Init vector for the fields's type IDs.
feltTypeIDs     = ones(1,Nfelt,'uint8');
% Set number of bytes to represent the field types number of bytes.
feltTypes_bytes = 1;

% Init vector for number of rows for each fields data.
feltNrows       = ones(1,Nfelt);
% Set number of bytes to represent the fields data number of rows.
feltNrows_bytes = 4;
% Init vector for number of cols for each fields data.
feltNcols       = ones(1,Nfelt);
% Set number of bytes to represent the fields data number of of cols.
feltNcols_bytes = 4; 

% Set number of bytes to represent the number of bytes of total message.
Nmessage_bytes  = 8;
% Set number of bytes to represent the struct ID.
ID_bytes        = 1;

% Loop through all fields.
for ii=1:Nfelt
    % Check for data type ID and resp. number of bytes of the fields's data-type.
    feltClass = class(v.(feltNames{ii}));
    switch feltClass
        case 'double'
            feltTypeIDs(ii) = 1;
            feltTypes(ii)   = 8;
        case 'single'
            feltTypeIDs(ii) = 2;
            feltTypes(ii)   = 4;
        case 'int8'
            feltTypeIDs(ii) = 3;
            feltTypes(ii)   = 1;
        case 'uint8'
            feltTypeIDs(ii) = 4;
            feltTypes(ii)   = 1;
        case 'int16'
            feltTypeIDs(ii) = 5;
            feltTypes(ii)   = 2;
        case 'uint16'
            feltTypeIDs(ii) = 6;
            feltTypes(ii)   = 2;
        case 'int32'
            feltTypeIDs(ii) = 7;
            feltTypes(ii)   = 4;
        case 'uint32'
            feltTypeIDs(ii) = 8;
            feltTypes(ii)   = 4;
        case 'int64'
            feltTypeIDs(ii) = 9;
            feltTypes(ii)   = 8;
        case 'uint64'
            feltTypeIDs(ii) = 10;
            feltTypes(ii)   = 8;
        case 'logical'
            feltTypeIDs(ii) = 11;
            feltTypes(ii)   = 1;            
        otherwise
            feltTypeIDs(ii) = 12;
            feltTypes(ii)   = 1;            
    end
    % Get the field names character-length.
    feltNamesLength(ii) = length(feltNames{ii});
    % Get the number of rows for each fields data.
    feltNrows(ii)       = size(v.(feltNames{ii}),1);
    % Get the number of cols for each fields data.
    feltNcols(ii)       = size(v.(feltNames{ii}),2);
end
% Get the number of values in the actual data in each field.
feltNdataOrig   = feltNrows(:).*feltNcols(:);
% Get the number of bytes to reresent the actual data in each fields.
feltNdata       = feltNdataOrig(:).*feltTypes(:);
% Get the total number of bytes to represent the actual data in v.
Ndata           = sum(feltNdata);

% Get the total byte size of the message.
Nmessage  = Nmessage_bytes + ID_bytes + Nfelt_bytes + ...
    Nfelt*feltNamesLength_bytes + Nchars + ...
    Nfelt*feltTypes_bytes + Nfelt*feltNrows_bytes + ...
    Nfelt*feltNcols_bytes + Ndata;
% Init message.
m = zeros(1,Nmessage,'uint8');
% Write the byte size of the message in the first data_bytes bytes of the message.
m(1:Nmessage_bytes)                      = typecast(uint64(Nmessage),'uint8');
idx                                      = Nmessage_bytes; 
% Write the ID of the message in the following ID_bytes bytes of the message.
m(idx+1:idx+ID_bytes)                    = typecast(uint8(ID),'uint8');
idx                                      = idx + ID_bytes;
% Write the number of fields in the following Nfelt_bytes bytes of the message.
m(idx+1:idx+Nfelt_bytes)                 = typecast(uint16(Nfelt),'uint8');
idx                                      = idx + Nfelt_bytes;
% Write the character-length of each field in the following 
% Nfelt*feltNamesLength_bytes bytes of the message.
m(idx+1:idx+Nfelt*feltNamesLength_bytes) = typecast(uint16(feltNamesLength),'uint8');
idx                                      = idx + Nfelt*feltNamesLength_bytes;
% Write the total number of characters in the following 
% Nchars bytes of the message.
m(idx+1:idx+Nchars)                      = typecast(uint8(chars),'uint8');
idx                                      = idx + Nchars;
% Write the data-type ID for each fields data type in the following 
% Nfelt*feltTypes_bytes bytes of the message.
m(idx+1:idx+Nfelt*feltTypes_bytes)       = typecast(uint8(feltTypeIDs),'uint8');
idx                                      = idx + Nfelt*feltTypes_bytes;
% Write the number of rows for each fields data in the following 
% Nfelt*feltNrows_bytes bytes of the message.
m(idx+1:idx+Nfelt*feltNrows_bytes)       = typecast(uint32(feltNrows),'uint8');
idx                                      = idx + Nfelt*feltNrows_bytes;
% Write the number of cols for each fields data in the following 
% Nfelt*feltNcols_bytes bytes of the message.
m(idx+1:idx+Nfelt*feltNcols_bytes)       = typecast(uint32(feltNcols),'uint8');
idx                                      = idx + Nfelt*feltNcols_bytes;
% Loop through all fields. 
for ii=1:Nfelt
    % If type ID is "logical" or "other" then make data to uint8 and then
    % typecast it. Else typecast directly.
    if feltTypeIDs(ii)>=11
        m(idx+1:idx+feltNdata(ii)) = typecast(uint8(reshape(v.(feltNames{ii}),1,feltNdataOrig(ii))),'uint8');
    else
        m(idx+1:idx+feltNdata(ii)) = typecast(reshape(v.(feltNames{ii}),1,feltNdataOrig(ii)),'uint8');
    end
    idx                            = idx + feltNdata(ii);
end