function [range,azim,elev,covSpher,layer,SNRecho,delay] =...
                                     unpkLidarData(LidarData,LidarParam,SS)

%#codegen
% Parameters
NrLayers  = LidarParam.NrOfLayers;
NdetLayer = LidarParam.NdetLayer;
Tcomp     = LidarParam.Delay;
Tscan     = LidarParam.Tscan;
% Get lidar data
rng = LidarData.au2_Range;
snr = LidarData.au1_SNR;
% Reshape data
ecNr  = 2;
echo  = logical([zeros(1,NdetLayer*NrLayers) ones(1,NdetLayer*NrLayers)]);
rng   = rng(1:ecNr*NdetLayer*NrLayers)';     
snr   = snr(1:ecNr*NdetLayer*NrLayers)';
layer = reshape(repmat(1:NrLayers,[NdetLayer 1]),[1 NdetLayer*NrLayers]);
horz  = repmat(1:NdetLayer,[1 NrLayers]);
% Set invalid values
rng(rng == 0)   = SS.u2_Invalid;
horz(rng == 0)  = SS.u2_Invalid;
layer(rng == 0) = SS.u1_Invalid;
snr(rng == 0)   = SS.u1_Invalid;

% Set the valid echo to be echo 2 and when echo 2 is invalid set echo 1 as 
% valid for those indices
rangeEchoQuant         = rng(echo);
rangeRestQuant         = rng(~echo);
SNRecho                = snr(echo);
SNRrest                = rng(~echo);
idxInv                 = rng(echo) == SS.u2_Invalid;
rangeEchoQuant(idxInv) = rangeRestQuant(idxInv);
SNRecho(idxInv)        = SNRrest(idxInv);

% Remove all detections 50 cm from sensor
rangeEchoQuant(rangeEchoQuant < 50) = SS.u2_Invalid;
horz(rangeEchoQuant < 50)           = SS.u2_Invalid;
layer(rangeEchoQuant < 50)          = SS.u1_Invalid;
SNRecho(rangeEchoQuant < 50)        = SS.u1_Invalid;

% Remove detections to simulate smaller FoV
% StartHorz = uint16(DetParam.ReduceAzim/DetParam.AzimStep+1);
% Range(Horz < StartHorz) = SS.u2_Invalid;
% Horz(Horz < StartHorz)  = SS.u2_Invalid;
% Layer(Horz < StartHorz) = SS.u1_Invalid;
% Echo(Horz < StartHorz)  = SS.u1_Invalid;
% SNR(Horz < StartHorz)   = SS.u1_Invalid;

% Get sensor data in speherical coordinates given data package format 
range = LidarParam.RngStep*single(rangeEchoQuant);
azim  = LidarParam.MaxHFoV - LidarParam.AzimStep*single(horz-1);
elev  = LidarParam.MaxVFoV - LidarParam.ElevStep/2 -...
             LidarParam.ElevStep*single(layer-1);
% Get delay w.r.t scanning and fixed delay.         
NrScanLayers            = 4;
scanlayer               = layer;
scanlayer(scanlayer==2) = 1;
scanlayer(scanlayer==3) = 2;
scanlayer(scanlayer==4) = 2;
scanlayer(scanlayer==5) = 3;
scanlayer(scanlayer==6) = 3;
scanlayer(scanlayer==7) = 4;
scanlayer(scanlayer==8) = 4;
delay = single(Tcomp + Tscan*(1 - 1/(NdetLayer*NrScanLayers)*...
               ((scanlayer - 1)*NdetLayer + horz)));

% Set invalid values to invalid or zero
range(rangeEchoQuant==SS.u2_Invalid) = SS.fl_Invalid;
azim(horz==SS.u2_Invalid)            = SS.fl_Invalid;
elev(layer==SS.u1_Invalid)           = SS.fl_Invalid;

covSpher = [LidarParam.RngStd^2 single(0) single(0); ...
           single(0) LidarParam.AzimStd^2 single(0); ...
           single(0) single(0) LidarParam.ElevStd^2];