function [u, ID] = ksonDecode(m)
%#codegen
% Set number of bytes to represent the total number of fields.
Nfelt_bytes = 2;

% Set number of bytes to represent the total number of character-lengths.
feltNamesLength_bytes = 2;

% Set number of bytes to represent the field types number of bytes.
feltTypes_bytes = 1;

% Set number of bytes to represent the fields data number of rows.
feltNrows_bytes = 4;

% Set number of bytes to represent the fields data number of cols.
feltNcols_bytes = 4; 

% Set number of bytes to represent the number of bytes of total message.
Nmessage_bytes  = 8;

% Set number of bytes to represent the struct ID.
ID_bytes        = 1;

Nmessage        = typecast(m(1:Nmessage_bytes),'uint64');
idx             = Nmessage_bytes;
ID              = typecast(m(idx+1:idx+ID_bytes),'uint8');
idx             = idx + ID_bytes;
Nfelt           = typecast(m(idx+1:idx+Nfelt_bytes),'uint16');
idx             = idx + Nfelt_bytes;
feltNamesLength = typecast(m(idx+1:idx+Nfelt*feltNamesLength_bytes),'uint16');
Nchars          = sum(feltNamesLength);
idx             = idx + Nfelt*feltNamesLength_bytes;
chars           = char(typecast(m(idx+1:idx+Nchars),'uint8'));
idx             = idx + Nchars;
feltTypeIDs     = typecast(m(idx+1:idx+Nfelt*feltTypes_bytes),'uint8');
idx             = idx + Nfelt*feltTypes_bytes;
feltNrows       = typecast(m(idx+1:idx+Nfelt*feltNrows_bytes),'uint32');
idx             = idx + Nfelt*feltNrows_bytes;
feltNcols       = typecast(m(idx+1:idx+Nfelt*feltNcols_bytes),'uint32');
idx             = idx + Nfelt*feltNcols_bytes;
idx             = uint32(idx);

% Init vector for the number of bytes of the fields's data-type.
feltTypes       = ones(1,Nfelt);
% Loop through all fields.
for ii=1:Nfelt
    % Check for data type ID for the fields's data-type.
    switch feltTypeIDs(ii)
        case uint8(1)
            feltTypes(ii)   = 8;
        case uint8(2)
            feltTypes(ii)   = 4;
        case uint8(3)
            feltTypes(ii)   = 1;
        case uint8(4)
            feltTypes(ii)   = 1;
        case uint8(5)
            feltTypes(ii)   = 2;
        case uint8(6)
            feltTypes(ii)   = 2;
        case uint8(7)
            feltTypes(ii)   = 4;
        case uint8(8)
            feltTypes(ii)   = 4;
        case uint8(9)
            feltTypes(ii)   = 8;
        case uint8(10)
            feltTypes(ii)   = 8;
        case uint8(11)
            feltTypes(ii)   = 1;
        otherwise
            feltTypes(ii)   = 1;
    end
end
% Get the number of values in the actual data in each field.
feltNdataOrig   = feltNrows(:).*feltNcols(:);
% Get the number of bytes to reresent the actual data in each fields.
feltNdata       = feltNdataOrig(:).*uint32(feltTypes(:));

idf = 0;
% Loop through all fields. 
for ii=1:Nfelt
    % Typecast data to orginal fromat from the uint8 byte stream.
    switch feltTypeIDs(ii)
        case 1
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'double'),feltNrows(ii),feltNcols(ii));
        case 2
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'single'),feltNrows(ii),feltNcols(ii));
        case 3
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'int8'),feltNrows(ii),feltNcols(ii));
        case 4
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'uint8'),feltNrows(ii),feltNcols(ii));
        case 5
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'int16'),feltNrows(ii),feltNcols(ii));
        case 6
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'uint16'),feltNrows(ii),feltNcols(ii));
        case 7
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'int32'),feltNrows(ii),feltNcols(ii));
        case 8
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'uint32'),feltNrows(ii),feltNcols(ii));
        case 9
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'int62'),feltNrows(ii),feltNcols(ii));
        case 10
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'uint64'),feltNrows(ii),feltNcols(ii));
        case 11
            u.(chars(idf+1:idf+feltNamesLength(ii))) = logical(reshape(typecast(m(idx+1:idx+feltNdata(ii)),'uint8'),feltNrows(ii),feltNcols(ii))); 
        otherwise
            u.(chars(idf+1:idf+feltNamesLength(ii))) = reshape(typecast(m(idx+1:idx+feltNdata(ii)),'uint8'),feltNrows(ii),feltNcols(ii));            
    end
    idx = idx + feltNdata(ii);
    idf = idf + feltNamesLength(ii);
end
