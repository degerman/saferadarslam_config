classdef Buffer < i_o.DataReader
    
    properties
        
        measurements
        
    end
    
    methods

        function self = Buffer(N)
                        
            self.index = 0;
            self.N = N;

            self.measurements = cell(1,N);
        end
        
        function b = isempty(self)
            
            b = self.index==0;
            
        end
        
        function reset(self)
            
            self.index = 0;
            self.measurements = cell(1,self.N);
        end
        
        function b = hasNext(self)
            
            b = self.index>0;
        
        end
                    
        function update(self,meas)
            
            if meas.isempty()
                disp('no data in measurement');
                return
            end
            self.measurements = circshift(self.measurements,1);
            %|x|[]|[]|[]| -> |[]|x|[]|[]|
            self.measurements{1} = meas;
            %|[]|x|[]|[]| -> |x|x|[]|[]|
            if self.index==self.N
                %disp('buffer full');
            else
                self.index = self.index + 1;
            end
        end
        
        function meas = getNext(self,offset)
            
            if self.isempty()
                disp('buffer empty');
            else
                if nargin==2
                    meas = self.measurements{max(1,self.index-offset)};
                else
                   meas = self.measurements{self.index}; 
                end
                self.measurements{self.index} = [];
                %|x|x|[]|[]| -> |x|[]|[]|[]|
                self.index = self.index - 1;
            end
        end
        
        function meas = getNextSync(self,time)

            %if measurement is not extracted unless it is older than time
            t_meas = self.measurements{self.index}.time;
            
            if time>=t_meas
                meas = self.getNext();
            else
                meas = [];
            end
        end
        
        function meas = peekSync(self,time)

            %find buffered measurement with nearest time
            t = [];
            for i=1:self.index
                t = [t,self.measurements{i}.time];
            end
            [~,ind] = min(abs(t-time));
            meas = self.measurements{ind};
            
        end
                
        function meas = peek(self)
            
            meas = self.measurements{self.index};
        end
                
    end
end