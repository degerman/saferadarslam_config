function [EgoParam, RefParam, LidarParamLf, LidarParamRf, RadarParamCf, DetParam, ...
    BoxParam, TrkParam, ObjParam] = TrackingParam
%% Ego Parameters
% Delay times.
EgoParam.LeapSeconds = 19;
EgoParam.RTdelay     = 0.02;
EgoParam.HSdelay     = 0.02;
% Length of ego signal buffer.
EgoParam.EgoBuffLen = 20;
% Number of sensor states.
EgoParam.NstateSensor = 6;
% Ego size.
EgoParam.dRef2Mid = 1.4;
EgoParam.Width    = 1.9;
EgoParam.Len      = 4.6;

% Ref Paramaters
RefParam.dRef2Mid = 1.4;
RefParam.Width    = 1.9;
RefParam.Len      = 4.6;

%% Left front Lidar Parameters
% Update rates.
LidarParamLf.Tscan    = 0.05;
LidarParamLf.Delay    = 0.07;
% Lidar mounting offsets.
LidarParamLf.PosX = single(3.712);
LidarParamLf.PosY = single(0.3);
LidarParamLf.PosZ = single(0.43);
% Max number of detections.
LidarParamLf.NrOfEcho   = 5;
LidarParamLf.NdetLayer  = 401;
LidarParamLf.NrOfLayers = 8;
LidarParamLf.Ndet       = LidarParamLf.NrOfEcho*LidarParamLf.NrOfLayers*...
                            LidarParamLf.NdetLayer;
% FoV.
LidarParamLf.MaxHFoV = 60*pi/180; 
LidarParamLf.MinHFoV = 0*pi/180;         
LidarParamLf.MaxVFoV = 1.4*pi/180;
LidarParamLf.MinVFoV = -1.8*pi/180;
% Resolution.
LidarParamLf.RngStep  = single(0.01);
LidarParamLf.AzimStep = single(0.15*pi/180);
LidarParamLf.ElevStep = single(0.4*pi/180);
% Standar deviations.
LidarParamLf.RngStd  = single(0.1);
LidarParamLf.AzimStd = LidarParamLf.AzimStep;
LidarParamLf.ElevStd = LidarParamLf.ElevStep;
% Clustering thresholds.
LidarParamLf.thresh_x  = 0.3; 
LidarParamLf.thresh_y  = 0.3; 
% Clustering thresholds for dbscan.
LidarParamLf.ClusDist  = 0.5;
LidarParamLf.ClusRngRt = 1;
% 1 for Lidar, 2 for Radar.
LidarParamLf.Type      = uint8(1);
%% Right front Lidar Parameters
% Update rates.
LidarParamRf.Tscan    = 0.05;
LidarParamRf.Delay    = 0.07;
% Lidar mounting offsets.
LidarParamRf.PosX = single(3.712);
LidarParamRf.PosY = single(-0.3);
LidarParamRf.PosZ = single(0.43);
% Max number of detections.
LidarParamRf.NrOfEcho   = 5;
LidarParamRf.NdetLayer  = 401;
LidarParamRf.NrOfLayers = 8;
LidarParamRf.Ndet       = LidarParamRf.NrOfEcho*LidarParamRf.NrOfLayers*...
                            LidarParamRf.NdetLayer;
% FoV.
LidarParamRf.MaxHFoV = 0; 
LidarParamRf.MinHFoV = -60*pi/180;       
LidarParamRf.MaxVFoV = 1.4*pi/180;
LidarParamRf.MinVFoV = -1.8*pi/180;
% Resolution.
LidarParamRf.RngStep  = single(0.01);
LidarParamRf.AzimStep = single(0.15*pi/180);
LidarParamRf.ElevStep = single(0.4*pi/180);
% Standar deviations.
LidarParamRf.RngStd  = single(0.1);
LidarParamRf.AzimStd = LidarParamRf.AzimStep;
LidarParamRf.ElevStd = LidarParamRf.ElevStep;
% Clustering thresholds.
LidarParamRf.thresh_x  = 0.3; 
LidarParamRf.thresh_y  = 0.3; 
% Clustering thresholds for dbscan.
LidarParamRf.ClusDist  = 0.5;
LidarParamRf.ClusRngRt = 1;
% 1 for Lidar, 2 for Radar.
LidarParamRf.Type      = uint8(1);

%% Front Center Radar Parameters
% Update rates.
RadarParamCf.Tscan    = 0.05;
RadarParamCf.Delay    = 0;
% Radar mounting offsets, zeros for radar simulator 
RadarParamCf.PosX = single(3.7);
RadarParamCf.PosY = single(-0.18);
RadarParamCf.PosZ = single(0.63);
% Max number of detections.
RadarParamCf.NrOfEcho   = 1;
RadarParamCf.NdetLayer  = 401;
RadarParamCf.NrOfLayers = 1;
% Max number of detections.
RadarParamCf.Ndet  = 401;
% FoV.
RadarParamCf.MaxHFoV = 70*pi/180; 
RadarParamCf.MinHFoV = -70*pi/180;       
RadarParamCf.MaxVFoV = 20*pi/180;
RadarParamCf.MinVFoV = -20*pi/180;
% Resolution.
RadarParamCf.RngStep  = single(0);
RadarParamCf.AzimStep = single(0);
RadarParamCf.ElevStep = single(0);
% Standar deviations.
RadarParamCf.RngStd  = single(0);
RadarParamCf.AzimStd = single(0);
RadarParamCf.ElevStd = single(0);
% Clustering thresholds.
RadarParamCf.thresh_x  = 0.3; 
RadarParamCf.thresh_y  = 0.3; 
% Clustering thresholds for dbscan.
RadarParamCf.ClusDist  = 0.5;
RadarParamCf.ClusRngRt = 1;
% 1 for Lidar, 2 for Radar.
RadarParamCf.Type      = uint8(2);

%% DetData parameters
DetParam.Ndet = 401;

%% Box fit parameters
BoxParam.Nmeas         = 18;
BoxParam.Nbox          = 100;
BoxParam.StdPosL       = single(0.1);
BoxParam.StdPosR       = single(0.3);
BoxParam.StdPosC       = single(0.3);
BoxParam.StdPosN       = single(1.5);
BoxParam.StdOrientL    = single(2*(pi/180));
BoxParam.StdOrientR    = single(20*(pi/180));
BoxParam.StdOrientC    = single(60*(pi/180));
BoxParam.StdOrientN    = single(60*(pi/180));
BoxParam.StdExt        = single(0.3);
BoxParam.LshapeFitting = uint8(1);
BoxParam.RoundingBox   = uint8(2);
BoxParam.CourseBox     = uint8(3);
BoxParam.NoBox         = uint8(4);

%% Track Parameters
% Max number of tracks
TrkParam.Ntrk = 40;
% Number of track states
TrkParam.Nstate = 7;
% Tracking on middlepoint. Set to 0 for corner tracking
TrkParam.MidpointTracking = 0;
% Box corners are numbered from 1-5 counter-clockwise.
% Number 1 is the rear right corner and 5 is middle-point.
% The corner data is stacked in the measurement vector Z with
% (x,y, and doppler) for each corner following in the 15 first locations.
% To get the index "cornIdx" for (x,y, and doppler) in Z for the 
% conrner number "cornNr", then write
% cornIdx = TrkParam.cornIdxInZ(:,cornNr)
TrkParam.cornIdxInZ = uint8([1 2 3;4 5 6;7 8 9;10 11 12;13 14 15]');
% An arbitrary corner number "cornNr" will be updated to a new number when
% the orinetation is changed by a multiple "hyp" of 90 degrees.
% To get an updated corner number "updtCornNr" in the hypothesis "hyp"
% (which correspons to adding (hyp-1)*pi/2 to the orientation), then write:
% updtCornNr = TrkParam.hypRotCornNr(cornNr,hyp)
TrkParam.hypRotCornNr = uint8([1 4 3 2;2 1 4 3;3 2 1 4;4 3 2 1;5 5 5 5]);
% The corners will swich locations in Z according to the map
% "TrkParam.hypRotZIdx" when rotating the box given an hypotesis "hyp".
% To get the hypothesis measurement of Z (corresposning to the
% hypothesis "hyp"), then write:
% hypZ = Z(TrkParam.hypRotZIdx(:,hyp));
TrkParam.hypRotZidx = uint8([1:18;...
                             4:12 1:3 13:16 18 17;...
                             7:12 1:6 13:18;...
                             10:12 1:9 13:16 18 17]');
% Threshold for close to FoV limit (Lidar)
TrkParam.FoVthr = 15*pi/180;
% Track initiation parameters
TrkParam.SpeedVarInit   = single(400);
TrkParam.HdgVarInit     = single((10*pi/180)^2);
TrkParam.YawRateVarInit = single(50);
TrkParam.WidthInit      = single(1.7);
TrkParam.LengthInit     = single(4);
% Track management parameters
TrkParam.MahalThr  = single(32);
TrkParam.MahalInit = single(100);
TrkParam.QualThr   = 2;
% Association parameters
TrkParam.MaxVar     = 16;
TrkParam.MaxSpeed   = 20;
TrkParam.MaxDist    = 1;
TrkParam.MaxYawRate = 1;
% Object states
TrkParam.new  = 1;
TrkParam.norm = 2;
TrkParam.lost = 6;
% Process noise values for Cordinated Turn model
TrkParam.PosXProcStdCT    = single(0);
TrkParam.PosYProcStdCT    = single(0);
TrkParam.SpeedProcStdCT   = single(1);
TrkParam.HeadProcStdCT    = single(0);
TrkParam.YawRateProcStdCT = single(0.5);
TrkParam.WidthProcStdCT   = single(0.1);
TrkParam.LengthProcStdCT  = single(0.1);
% Process noise values for Constant Velocity model
TrkParam.PosXProcStdCV    = single(0);
TrkParam.PosYProcStdCV    = single(0);
TrkParam.SpeedProcStdCV   = single(0.5);
TrkParam.HeadProcStdCV    = single(2);
TrkParam.YawRateProcStdCV = single(0);
TrkParam.WidthProcStdCV   = single(0.1);
TrkParam.LengthProcStdCV  = single(0.1);
% Allow for setting object to stationary.
TrkParam.SetStatObj = 0;
% Stationary track threshold.
TrkParam.StatThr = 0.3;
% Classification parameters.
TrkParam.PedSizeThr = single(2);
TrkParam.PedSpdThr  = single(10);
% Initialization parameters. 
TrkParam.PedRangeThr = single(50);
% Number of objects to send on CAN and visualize.
ObjParam.Nobj = 8;
% Number of objects detections to send on CAN and visualize.
ObjParam.NdetObj = 50;