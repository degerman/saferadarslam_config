classdef ZMQ_test < matlab.unittest.TestCase
    
    properties
        
        ctx
        socket1
        socket2
        socket3
        poller
        
    end
        
    methods(TestMethodSetup)
        
        function setupTest(self)
            
            %clear java
            
            javaclasspath(getenv('JAR_FILE'));
            
            import org.zeromq.*
            
            %set subscriber socket for subscibe
            self.ctx     = ZMQ.context(1);
            self.socket1  = self.ctx.socket(ZMQ.SUB);
            self.socket1.connect('tcp://127.0.0.1:7577');
            self.socket1.subscribe('');
            self.socket1.setReceiveTimeOut(100);
                        
            self.socket2 = self.ctx.socket(ZMQ.SUB);
            self.socket2.connect('tcp://127.0.0.1:7579');
            self.socket2.subscribe('');
            self.socket2.setReceiveTimeOut(100);
            
            self.poller = self.ctx.poller();
            self.poller.register(self.socket1, self.ctx.poller.POLLIN);
            self.poller.register(self.socket2, self.ctx.poller.POLLIN);
            
            %the publisher socket cannot be set up here, it must be set up
            %in the subfunction

        end
    end
        
    methods(Test)
        
        function testRecieve(self)
            
            % %Set up sockets for publish.
            system([getenv('PYTHON_BIN_LOCATION') ' +i_o/+test/zmq_test_pub.py &']);
            
            msg = 'x=0';
            M = [];
            %read from socket
            while ~strcmp(msg,'end')
                M = [M;msg];
                msg    = native2unicode(self.socket1.recv(0))';
            end
            
            %check all the numbers
            self.assertEqual(M(1,:),['x=0']);
            self.assertEqual(M(2,:),['x=1']);
            self.assertEqual(M(10,:),['x=9']);
            
        end
        
        function testPoller(self)
            
            % Set up sockets for publish.
            system([getenv('PYTHON_BIN_LOCATION') ' +i_o/+test/zmq_test_poll.py &']);

            M1 = [];
            M2 = [];

            ready = true;
            while ready
                rc = self.poller.poll(1);
                if self.poller.pollin(0)
                    msg1 = native2unicode(self.socket1.recv(0))';
                    M1 = [M1;msg1];
                    if strcmp(msg1,'end1')
                        ready = false;
                    end
                end
                if self.poller.pollin(1)
                    msg2 = native2unicode(self.socket2.recv(0))';
                    M2 = [M2;msg2];
                end
            end
            self.socket1.close();
            self.socket2.close();
            
            %check all the numbers
            self.assertEqual(M1(1,:),['x1=1']);
            self.assertEqual(M1(2,:),['x1=2']);
            self.assertEqual(M1(9,:),['x1=9']);
            self.assertEqual(M2(1,:),['x2=1']);
            self.assertEqual(M2(2,:),['x2=2']);
            self.assertEqual(M2(9,:),['x2=9']);
        end
        
        function testSend(self)
            
            import org.zeromq.*

            %set up socket for publish
            self.socket3 = self.ctx.socket(ZMQ.PUB);
            self.socket3.bind('tcp://*:7578');

            %set up socket for subscribe
            system([getenv('PYTHON_BIN_LOCATION') ' +i_o/+test/zmq_test_sub.py &']);
            pause(0.1);
            for x=1:10
                self.socket3.send(unicode2native(['x=' num2str(x)]));
            end
            self.socket3.close();
            
            %sent data is written in a file
            id = fopen('+i_o/+test/testfile.txt');
            txt = native2unicode(fread(id)')
            fclose(id);
            self.assertEqual(txt,'x=1x=2x=3x=4x=5x=6x=7x=8x=9x=10');
            
        end
        
    end
    
end
