classdef testBuffer < matlab.unittest.TestCase
    properties
        B
        EP
    end
    
    methods(TestMethodSetup)
        function setupTests(self)
            
            addpath('../saferadarslam');
            
        end
    end

        
    methods (Test)
        function test(self)

%            self.EP = slam.i_o.EgoProvider();
            
            self.B = i_o.Buffer(5);
            self.assertTrue(self.B.isempty());
            self.assertFalse(self.B.hasNext);
            m = slam.sensor.Measurement(false,1);
            m.time = 1;
            self.B.update(m);
            self.assertTrue(self.B.hasNext);
            
            %assert sync is working
            m = self.B.getNextSync(0);
            self.assertEmpty(m);
            m = self.B.getNextSync(1);
            self.assertNotEmpty(m);
            self.assertFalse(self.B.hasNext);
            
            %assert right order extraction
            m1 = slam.sensor.Measurement(false,1);
            m1.time = 1;
            m2 = slam.sensor.Measurement(false,1);
            m2.time = 1.1;
            self.B.update(m1);
            self.B.update(m2);
            m = self.B.getNext();
            self.assertEqual(m1.time,m.time);
            m = self.B.getNext();
            self.assertNotEmpty(m);
            
            self.B.reset();
            %test overflow
            for i=1:6
                m.time = 1 +i*0.1;
                self.B.update(m);
            end
            
        end
    end
end
