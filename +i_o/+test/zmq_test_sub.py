import zmq
import time

file = open('./+test/testfile.txt','w') 
 
context = zmq.Context()

socket = context.socket(zmq.SUB)

socket.connect('tcp://127.0.0.1:7578')

socket.setsockopt_string(zmq.SUBSCRIBE,'')

socket.setsockopt(zmq.RCVTIMEO,500)

iter = 0;

while iter<10:

    try:
        s = socket.recv_string()
        print(s)
        file.write(s)
    except zmq.error.Again:
        pass
        
    iter = iter + 1
    
file.close() 

