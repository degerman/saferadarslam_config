import zmq
import time

context = zmq.Context()

socket1 = context.socket(zmq.PUB)
socket2 = context.socket(zmq.PUB)

socket1.bind('tcp://*:7577')
socket2.bind('tcp://*:7579')

#wait for recieve socket
time.sleep(0.25)

for x in range(1, 10):
    s = "x1=%d" % (x)
    socket1.send_string(s)
    s = "x2=%d" % (x)
    socket2.send_string(s)

socket1.send_string('end1')
socket2.send_string('end2')