import zmq
import time

context = zmq.Context()

socket = context.socket(zmq.PUB)

socket.bind('tcp://*:7577')

#wait for recieve socket
time.sleep(0.25)

for x in range(1, 10):
    s = "x=%d" % (x)
    socket.send_string(s)

socket.send_string('end')