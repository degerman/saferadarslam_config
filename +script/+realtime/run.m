
[f1,f2]=fileparts(pwd);
if strcmp(f2,getenv('SLAM_REPO'))
    %do nothing
elseif strcmp(f2,getenv('SLAM_CONFIG_REPO'))
    cd(fullfile(f1,getenv('SLAM_REPO')))
elseif ~isempty(dir(getenv('SLAM_REPO')))
    cd(getenv('SLAM_REPO'));
else
    error('go to the slam root folder');
end

close all

data_param_json    = '../saferadarslam_config/param/realtime/fusion_param.json';
% sensor_param_json  = '../saferadarslam_config/param/realtime/multisensor_param_radar_only.json';
sensor_param_json  = '../saferadarslam_config/param/realtime/multisensor_param.json';
slam_param_json    = '../saferadarslam_config/param/realtime/slam_param.json';

jar_file = getenv('JAR_FILE');

runner = i_o.Runner(data_param_json,sensor_param_json,slam_param_json,jar_file);

runner.initFusion();

%runner.initSensorConfiguration('standard');

runner.loadSensorData();

runner.setupSensorsFusion();

%runner.mapping();

%runner.measurements()


% 
% sensor_param    = slam.i_o.readJsonFile(['+param' filesep '+realtime' filesep 'p2_radar.json']);
% 
% sensors{1} = slam.sensor.RadarSensor(sensor_param.sensors{1},api.TCPreader(sensor_param.sensors{1}.port));
% 
% sensors{2} = slam.sensor.EgoSensor(sensor_param.sensors{2},api.TCPreader(sensor_param.sensors{2}.port));
% 
% sensorQueue   = slam.i_o.SensorQueue(sensors);
% 
% fusion_engine = slam.i_o.Fusion(sensorQueue.sensors,sensor_param);
% 
% max_iter = 100;
% iter = 1;
% 
% figure(1);
% vehicle = slam.simulator.Vehicle();
% fusion_engine.theGrid.visualize2D();
% hold on;
% title('2D view of grid map, including sub-maps') 
% view(-10,40);
% axis equal
% hold off
% 
% while sensorQueue.hasNext() && iter < max_iter
% 
%     ego = slam.localization.EgoState([0,0,0]',[0,0,0]',0,[0,0,0]');
%     ego.time = posixtime(datetime('now'))-3600;
% 
%     fusion_engine.egoProvider.update(ego);
%     
%     pause(0.1)
%     meas = sensorQueue.getNextMeasurement();
% 
%     meas.SNR
%     
%     if isa(meas,'slam.sensor.RadarMeasurement') || ...
%             isa(meas,'slam.sensor.LidarMeasurement')
%         if ~isempty(fusion_engine.egoProvider.ego)
%             vehicle.visualize2D([fusion_engine.egoProvider.ego.pos;...
%                 fusion_engine.egoProvider.ego.angle]);
% %            sensors{meas.sensor_id}.visualizeMeasurements(...
% %                sensors{meas.sensor_id}.inverseMeasurementFunction(meas,...
% %                fusion_engine.egoProvider.ego));
%             
%         end
%     end
%     drawnow;
% 
%     if fusion_engine.map(meas)
% 
%         disp(['Mapping iteration: ' num2str(iter)]);
%         iter = iter + 1;
%         fusion_engine.theGrid.visualize3D([0.1,0.5,2,5],[fusion_engine.theGrid.limits(1,:), fusion_engine.theGrid.limits(2,:), fusion_engine.theGrid.limits(3,:)]);
% 
%     end
%     
% end
% 
% %fusion_engine.theGrid.visualize3D([0.1,0.5,2,5],[fusion_engine.theGrid.limits(1,:), fusion_engine.theGrid.limits(2,:), fusion_engine.theGrid.limits(3,:)]);
% 
