clear all

sensor_param    = i_o.readJsonFile(['param' filesep 'realtime' filesep 'p2_radar.json']);

jarfile = '../jeromq-master/target/jeromq-0.4.4-SNAPSHOT.jar';
radar_reader{1} = api.RadarReader(jarfile, ['tcp://192.168.4.105:', num2str(sensor_param.sensors{1}.port)]);
radar_reader{2} = api.RadarReader(jarfile, ['tcp://192.168.4.105:', num2str(sensor_param.sensors{2}.port)]);
radar_reader{3} = api.RadarReader(jarfile, ['tcp://192.168.4.105:', num2str(sensor_param.sensors{3}.port)]);
s{1} = scatter(-1,0);
hold on
s{2} = scatter(-1,0);
s{3} = scatter(-1,0);
hold off
axis([-1 30 -10 10])
while true
    for i = 1:3
        [meas,meta] = radar_reader{i}.getNextMeasurement;
        
        if ~strcmp(meas,'no_data')
            disp('new_data')
            disp(i)
            disp(meta.chip_id)
            r = meas.afl_Range;
            rr = meas.afl_RangeRate;
            u = meas.afl_U;
            
            %         scatter(r-sqrt(1-u.^2), u);
            s{i}.XData = r;
            s{i}.YData = r.*u;
            title(meas.fl_CurrTime);
            drawnow
            
        end
    end
end