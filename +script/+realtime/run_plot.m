
clear all
close all

sensor_param    = i_o.readJsonFile(['param' filesep 'realtime' filesep 'p2_radar.json']);

jarfile = '../jeromq-master/target/jeromq-0.4.4-SNAPSHOT.jar';
radar_reader1 = api.RadarReader(jarfile, ['tcp://192.168.4.105:', num2str(sensor_param.sensors{1}.port)]);
radar_reader2 = api.RadarReader(jarfile, ['tcp://192.168.4.105:', num2str(sensor_param.sensors{2}.port)]);
radar_reader3 = api.RadarReader(jarfile, ['tcp://192.168.4.105:', num2str(sensor_param.sensors{3}.port)]);

sensors{1} = slam.sensor.RadarSensor(sensor_param.sensors{1},radar_reader1);
sensors{2} = slam.sensor.RadarSensor(sensor_param.sensors{2},radar_reader2);
sensors{3} = slam.sensor.RadarSensor(sensor_param.sensors{3},radar_reader3);

sensors{4} = slam.sensor.EgoSensor(sensor_param.sensors{4},[]);

sensorQueue   = slam.i_o.SensorQueue(sensors);

fusion_engine = slam.i_o.Fusion(sensorQueue.sensors,sensor_param);

max_iter = 100;
iter = 1;

figure(1);
vehicle = slam.simulator.Vehicle();
fusion_engine.theGrid.visualize2D();
hold on;
title('2D view of grid map, including sub-maps') 
view(-10,40);
axis equal
hold off

while sensorQueue.hasNext() && iter < max_iter

    ego = slam.localization.EgoState([0,0,0]',[0,0,0]',0,[0,0,0]');
    ego.time = posixtime(datetime('now'))-3600;

    fusion_engine.egoProvider.update(ego);
    
    pause(0.1)
    meas = sensorQueue.getNextMeasurement();

    meas.SNR
    
    scatter(meas.range.*sqrt(1-meas.u.^2),meas.range.*meas.u)
    axis([-10 80 -20 20])
    drawnow;
    
end

