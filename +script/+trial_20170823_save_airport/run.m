%make sure we are in correct folder
[f1,f2]=fileparts(pwd);
if strcmp(f2,getenv('SLAM_REPO'))
    %do nothing
elseif strcmp(f2,getenv('SLAM_CONFIG_REPO'))
    cd(fullfile(f1,getenv('SLAM_REPO')))
elseif ~isempty(dir(getenv('SLAM_REPO')))
    cd(getenv('SLAM_REPO'));
else
    error('go to the slam root folder');
end

clear all
close all

%%

file_param_json    = ['../' getenv('SLAM_CONFIG_REPO') '/param/trial_20170823_save_airport/metal_poles_straight_param.json'];
sensor_param_json  = ['../' getenv('SLAM_CONFIG_REPO') '/param/trial_20170823_save_airport/multisensor_param.json'];
slam_param_json    = ['../' getenv('SLAM_CONFIG_REPO') '/param/trial_20170823_save_airport/slam_param.json'];
jar_file           = getenv('JAR_FILE');
adress             = 'tcp://*:7585';

runner = i_o.Runner(file_param_json,sensor_param_json,slam_param_json,jar_file,adress);

runner.initFusion();

runner.initSensorConfiguration('lidar_only');

file_index = 1;
runner.loadSensorData(file_index);

runner.setupSensorsFusion();

runner.mapping()

GRID = runner.fusion_engine.map;
anchor = runner.getAnchor();
runner.fusion_engine.convertGrid();

%%

%use_case = 'radar_only';
use_case = 'lidar_only';
index         = 4;
[sensors,param,ego_index,poles] = script.trial_20170823_save_airport.read_data(file_param,sensor_param,index,use_case);

sensorQueue   = slam.i_o.SensorQueue(sensors);
%sensorQueue.fastForward(97);
sensorQueue.fastForward(201);

fusion_engine = slam.i_o.Fusion(sensorQueue.sensors,param);

max_iter = 1500;
iter = 1;

mapping_visualizer = slam.util.MappingVisualizer(fusion_engine.map,sensorQueue.sensors);
mapping_visualizer.addPoles(poles);

while sensorQueue.hasNext() && iter < max_iter
    
    meas = sensorQueue.getNextMeasurement();
    
   mapping_visualizer.update(meas,fusion_engine.egoProvider.ego);

    fusion_engine.buffer(meas) % add to buffer

    if fusion_engine.mapping()
        disp(['Mapping iteration: ' num2str(iter)]);
        iter = iter + 1;
        
    end
    
end

GRID = fusion_engine.map;

fusion_engine.convertGrid();

visualizer = slam.util.Visualizer(sensors);

visualizer.initGrid(GRID);

visualizer.addPoles(poles);
visualizer.addTrajectory(sensors{ego_index}.sensorData.getTrajectory());

%%
% 
% anchor = [sensors{ego_index}.sensorData.lon_anchor,sensors{ego_index}.sensorData.lat_anchor];
% 
% index = 4;
% [sensors,param,ego_index,poles] = script.trial_20170823_save_airport.read_data(file_param,sensor_param,index,use_case,anchor);
% sensorQueue   = slam.i_o.SensorQueue(sensors);
% sensorQueue.fastForward(97);
% 
% fusion_engine.reset();
% fusion_engine.logger.reset();
% 
% visualizer = slam.util.Visualizer(sensors);
% 
% visualizer.initGrid(GRID);
% 
% visualizer.addPoles(poles);
% visualizer.addTrajectory(sensors{ego_index}.sensorData.getTrajectory());
% 
% %flush many measurements in the beginning
% %fast forward index 1:100
% %fast forward index 3:421.5
% %fast forward index 4: 655
% 
% sensorQueue.fastForward(655);
% 
% fusion_engine.egoProvider.particle_filter_init = false;
%     
% iter = 1;
% 
% max_iter = 80; %lidar
% 
% %index 1:max_iter:140
% %index 3:max_iter:100
% %index 4:max_iter;
% max_iter = 100; %radar
% %max_iter = 220;
% 
% while sensorQueue.hasNext() && iter < max_iter
% 
%     meas = sensorQueue.getNextMeasurement();
%     
%     if fusion_engine.initParticleFilter(meas,visualizer)
%         
%         if fusion_engine.filter(meas)
%             
%             drawnow;
%             
%             ground_truth = sensors{ego_index}.sensorData.getMeasurementSync(meas.time);
%             fusion_engine.logger.putReference([ground_truth.pos;ground_truth.angle]);
%             fusion_engine.logger.putTime(meas.time);
%             
%             visualizer.update(fusion_engine.logger,fusion_engine.particleFilter);
%             
%             disp(['Filtering iteration: ' num2str(iter)]);
%             iter = iter + 1;
%             %fusion_engine.particleFilter.state_estimate.angle
% 
%         end
%     end
%     
% end
% 
% fusion_engine.plot_results();
