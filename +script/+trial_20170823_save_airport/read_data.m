function [sensors,sensor_param,ego_index,poles] = read_data(file_param,sensor_param,index,version,anchor)

%setting up the readers

radar_file      = fullfile(file_param.radar_data.folder,file_param.radar_data.sub_folders{1},file_param.radar_data.file_names{index});
lidar_file      = fullfile(file_param.lidar_data.folder,file_param.lidar_data.sub_folders{1},file_param.lidar_data.file_names{index});
reference_file  = fullfile(file_param.reference_data.folder,file_param.reference_data.sub_folders{1},file_param.reference_data.file_names{index});
poles_file      = fullfile(file_param.target_data.folder,file_param.target_data.sub_folders{1},file_param.target_data.file_names{1});

radar_reader    = api.RadarFileReaderNew(radar_file,file_param.radar_data.UTC_time_offset);
lidar_reader    = api.LidarFileReaderRaw(lidar_file,file_param.lidar_data.UTC_time_offset);
if nargin==3
    ego_reader      = api.EgoFileReader(reference_file, [anchor(1),anchor(2)]);
else
    ego_reader = api.EgoFileReader(reference_file);
end
poles           = slam.util.MetalPoles(poles_file,ego_reader);
%setting up the sensors

sensor_param.occupancy_grid.limits = file_param.limits(:)';

ego_index = 3;
if strcmp(version,'radar_only')
    %radar only:
    sensor_param.sensors = sensor_param.sensors([1,3]);
    sensor_param.measurement_grids = sensor_param.measurement_grids(1);
    ego_index = 2;
elseif strcmp(version,'lidar_only')
    %lidar only
    sensor_param.sensors = sensor_param.sensors([2,3]);
    sensor_param.measurement_grids = sensor_param.measurement_grids(2);
    sensor_param.sensors{1}.sensor_id = 1;
    sensor_param.measurement_grids{1}.grid_index = 1;
    ego_index = 2;
end

for i = 1:length(sensor_param.sensors)
    if strcmp(sensor_param.sensors{i}.sensor_type,'radar')
        sensors{i} = slam.sensor.RadarSensor(sensor_param.sensors{i});
        sensors{i}.addSource(radar_reader);
    elseif strcmp(sensor_param.sensors{i}.sensor_type,'lidar')
        sensors{i} = slam.sensor.LidarSensor(sensor_param.sensors{i});
        sensors{i}.addSource(lidar_reader);
    elseif strcmp(sensor_param.sensors{i}.sensor_type,'ego')
        sensors{i} = slam.sensor.EgoSensor(sensor_param.sensors{i});
        sensors{i}.addSource(ego_reader);
    else
        error('uknown sensor typ in setup file');
    end
end