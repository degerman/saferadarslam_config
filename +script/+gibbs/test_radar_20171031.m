if 1
%simulator is only radar
[f1,f2]=fileparts(pwd);
if strcmp(f2,'saferadarslam')
    %do nothing
elseif strcmp(f2,'saferadarslam_config')
    cd(fullfile(f1,'saferadarslam'))
elseif ~isempty(dir('saferadarslam'))
    cd('saferadarslam');
else
    error('go to the saferadarslam folder (root)');
end

clear all
close all

show_plots = 1;
max_iter = 50;
iter = 1;
index = 1;
version = 'radar_only';
%use_case = 'lidar_only';
end
file_param_json    = '../saferadarslam_config/param/trial_20170823_save_airport/metal_poles_straight_param_pontus.json';
sensor_param_json  = '../saferadarslam_config/param/trial_20170823_save_airport/multisensor_param.json';
slam_param_json    = '../saferadarslam_config/param/trial_20170823_save_airport/slam_param.json';


% file_param_json    = '../saferadarslam_config/param/trial_20171031_save_airport/metal_poles_straight_param_pontus.json';
% sensor_param_json  = '../saferadarslam_config/param/trial_20171031_save_airport/multisensor_param.json';
% slam_param_json    = '../saferadarslam_config/param/trial_20171031_save_airport/slam_param.json';

runner = slam.i_o.Runner(file_param_json,sensor_param_json,slam_param_json);

runner.initFusion();

runner.initSensorConfiguration('P2_only');

file_index = 3;

runner.loadSensorData(file_index)

runner.setupSensorsFusion();
if 1
runner.mapping()

%%
end
runner.fusion_engine.map = map;
% anchor = runner.getAnchor();

runner.fusion_engine.logger.reset();

 runner.initVisualizer(map);

runner.initSensorConfiguration('P2_only');
file_index = 3;
runner.loadSensorData(file_index,anchor);

runner.setupSensorsFusion();
%fast-farword to start of poles
runner.sensorQueue.fastForward(runner.data_param.start_time(file_index)+1);

runner.localization()

runner.fusion_engine.plot_results();
