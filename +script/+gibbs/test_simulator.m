%simulator is only radar
clear all

show_plots = 1;
max_iter = 50;
iter = 1;

visual     = usejava('jvm') && show_plots;
file_name  = ['+param' filesep '+simulator' filesep 'simulation_param_singlesensor.json'];
signal     = slam.simulator.Scenario(file_name);
fusion_engine = slam.i_o.Fusion(signal.sensorQueue.sensors,signal.param);



while signal.hasNext() && iter < max_iter
    
    meas = signal.getNextMeasurementInQueue();
    
    if fusion_engine.mapping(meas)
        disp(['Mapping iteration: ' num2str(iter)]);
        iter = iter + 1;
    end
end