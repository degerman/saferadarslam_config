%make sure we are in correct folder
[f1,f2]=fileparts(pwd);
if strcmp(f2,getenv('SLAM_REPO'))
    %do nothing
elseif strcmp(f2,getenv('SLAM_CONFIG_REPO'))
    cd(fullfile(f1,getenv('SLAM_REPO')))
elseif ~isempty(dir(getenv('SLAM_REPO')))
    cd(getenv('SLAM_REPO'));
else
    error('go to the slam root folder');
end
clear all
close all


data_param_json    = ['../' getenv('SLAM_CONFIG_REPO') '/param/simulator/simulation_param.json'];
sensor_param_json  = ['../' getenv('SLAM_CONFIG_REPO') '/param/simulator/single_sensor_param.json'];
%sensor_param_json  = ['../' getenv('SLAM_CONFIG_REPO') '/param/simulator/multi_sensor_param.json'];
slam_param_json    = ['../' getenv('SLAM_CONFIG_REPO') '/param/simulator/slam_param.json'];
jar_file = getenv('JAR_FILE');
adress = 'tcp://*:7585';

runner = i_o.Runner(data_param_json,sensor_param_json,slam_param_json,jar_file);

runner.initFusion();

runner.loadSensorData();

runner.setupSensorsFusion();

% runner.mapping(true);

% runner.fusion_engine.map.visualize3D;

% runner.sensorQueue.reset();

% runner.localization(true);



% visualizer = slam.util.Visualizer(signal.sensorQueue.sensors);
% visualizer.initGrid(fusion_engine.map);
% 
% fusion_engine.convertGrid();
% fusion_engine.reset();
% 
% signal.reset();
% iter = 1;
% 
% while signal.hasNext() && iter < max_iter
% 
%     meas = signal.getNextMeasurementInQueue();
%     
%     if fusion_engine.initParticleFilter(meas,visualizer)
%         
%         fusion_engine.buffer(meas);
%         
%         if fusion_engine.filter()
%             
%             time = fusion_engine.particleFilter.last_update_time;
%             ground_truth = signal.dataSource.getState(time);
%             fusion_engine.logger.putReference([ground_truth.pos;ground_truth.angle]);
%             fusion_engine.logger.putTime(time);
%             fusion_engine.logger.putMeasurement(meas);
%             fusion_engine.logger.putData([fusion_engine.particleFilter.getEstimate().pos;fusion_engine.particleFilter.getEstimate().angle]);
%             fusion_engine.logger.putParticles(fusion_engine.particleFilter);
%             
%             visualizer.update(fusion_engine.logger,fusion_engine.particleFilter);
%             
%             disp(['Filtering iteration: ' num2str(iter)]);
%             iter = iter + 1;
%         end
%     end
%     
% end
% 
% fusion_engine.logger.landmarks = signal.dataSource.road.landmarks;
% fusion_engine.plot_results();
