%make sure we are in correct folder
[f1,f2]=fileparts(pwd);
if strcmp(f2,SLAM_REPO)
    %do nothing
elseif strcmp(f2,SLAM_CONFIG_REPO)
    cd(fullfile(f1,SLAM_REPO))
elseif ~isempty(dir(SLAM_REPO))
    cd(SLAM_REPO);
else
    error('go to the slam root folder');
end

clear all
close all

%%

file_param_json    = ['../' getenv('SLAM_CONFIG_REPO') '/param/trial_20171031_save_airport/P2_R1_straight_in_T_intersection.json'];
sensor_param_json  = ['../' getenv('SLAM_CONFIG_REPO') '/param/trial_20171031_save_airport/multisensor_param.json'];
slam_param_json    = ['../' getenv('SLAM_CONFIG_REPO') '/param/trial_20171031_save_airport/slam_param.json'];

runner = slam.i_o.Runner(file_param_json,sensor_param_json,slam_param_json);

runner.initFusion();

runner.initSensorConfiguration('P2_only');

file_index = 3;
runner.loadSensorData(file_index);

runner.setupSensorsFusion();


runner.mapping()
GRID = runner.fusion_engine.map;
anchor = runner.getAnchor();
runner.fusion_engine.convertGrid();

%%

runner.fusion_engine.logger.reset();

runner.initVisualizer(GRID);

runner.initSensorConfiguration('P2_only');
file_index = 3;
runner.loadSensorData(file_index,anchor);

runner.setupSensorsFusion();
%fast-farword to start of poles
runner.sensorQueue.fastForward(runner.data_param.start_time(file_index)+1);

runner.localization()

runner.fusion_engine.plot_results();

