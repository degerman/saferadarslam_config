function [sensors,sensor_param,ego_index,poles] = read_data(file_param,sensor_param,index,anchor)

%setting up the readers

P2_file      = fullfile(file_param.P2_data.folder,file_param.P2_data.sub_folders{1},file_param.P2_data.file_names{index});
R1_file      = fullfile(file_param.R1_data.folder,file_param.R1_data.sub_folders{1},file_param.R1_data.file_names{index});
lidar_file   = fullfile(file_param.R1_data.folder,file_param.lidar_data.sub_folders{1},file_param.lidar_data.file_names{index});
reference_file  = fullfile(file_param.reference_data.folder,file_param.reference_data.sub_folders{1},file_param.reference_data.file_names{index});
poles_file   = fullfile(file_param.target_data.folder,file_param.target_data.sub_folders{1},file_param.target_data.file_names{1});

P2_reader    = api.RadarFileReaderNew(P2_file,file_param.P2_data.UTC_time_offset);
R1_reader    = api.RadarFileReaderNewR1(R1_file,file_param.R1_data.UTC_time_offset);
if index~=3
    lidar_reader = api.LidarFileReaderRaw(lidar_file,file_param.lidar_data.UTC_time_offset);
end

if nargin==4
    ego_reader = api.EgoFileReader(reference_file, [anchor(1),anchor(2)]);
else
    ego_reader = api.EgoFileReader(reference_file);
end

poles           = slam.util.MetalPoles(poles_file,ego_reader);
%setting up the sensors

sensor_param.occupancy_grid.limits = file_param.limits(:)';
ego_index=4;

version = 'P2_R1';
if strcmp(version,'P2_only')
    %radar only:
    sensor_param.sensors = sensor_param.sensors([1,4]);
    sensor_param.measurement_grids = sensor_param.measurement_grids(1);
    ego_index = 2;
elseif strcmp(version,'R1_only')
    %lidar only
    sensor_param.sensors = sensor_param.sensors([2,4]);
    sensor_param.measurement_grids = sensor_param.measurement_grids(2);
    sensor_param.sensors{1}.sensor_id = 1;
    sensor_param.measurement_grids{1}.grid_index = 1;
    ego_index = 2;
elseif strcmp(version,'P2_R1')
    %lidar only
    sensor_param.sensors = sensor_param.sensors([1,2,4]);
    sensor_param.measurement_grids = sensor_param.measurement_grids(1:2);
    ego_index = 3;
elseif strcmp(version,'lidar_only')
    %lidar only
    sensor_param.sensors = sensor_param.sensors([3,4]);
    sensor_param.measurement_grids = sensor_param.measurement_grids(3);
    sensor_param.sensors{1}.sensor_id = 1;
    sensor_param.measurement_grids{1}.grid_index = 1;
    ego_index = 2;
end

for i = 1:length(sensor_param.sensors)
    if strcmp(sensor_param.sensors{i}.sensor_type,'radar_P2')
        sensors{i} = slam.sensor.RadarSensor(sensor_param.sensors{i});
        sensors{i}.addSource(P2_reader);
    elseif strcmp(sensor_param.sensors{i}.sensor_type,'radar_R1')
        sensors{i} = slam.sensor.RadarSensor(sensor_param.sensors{i});
        sensors{i}.addSource(R1_reader);
    elseif strcmp(sensor_param.sensors{i}.sensor_type,'lidar')
        sensors{i} = slam.sensor.LidarSensor(sensor_param.sensors{i});
        sensors{i}.addSource(lidar_reader);
    elseif strcmp(sensor_param.sensors{i}.sensor_type,'ego')
        sensors{i} = slam.sensor.EgoSensor(sensor_param.sensors{i});
        sensors{i}.addSource(ego_reader);
    else
        error('uknown sensor typ in setup file');
    end
end