%make sure we are in correct folder
[f1,f2]=fileparts(pwd);
if strcmp(f2,SLAM_REPO)
    %do nothing
elseif strcmp(f2,SLAM_CONFIG_REPO)
    cd(fullfile(f1,SLAM_REPO))
elseif ~isempty(dir(SLAM_REPO))
    cd(SLAM_REPO);
else
    error('go to the slam root folder');
end

clear all
close all

data_param_json    = '../saferadarslam_config/param/realtime/realtime_fusion.json';
sensor_param_json  = '../saferadarslam_config/param/realtime/multisensor_param.json';
slam_param_json    = '../saferadarslam_config/param/realtime/slam_param.json';

runner = slam.i_o.Runner(data_param_json,sensor_param_json,slam_param_json);

runner.initFusion();

runner.initSensorConfiguration('allsensors');

runner.loadSensorData();

runner.setupSensorsFusion();


radar.getNextMeasurement
ego.getNextMeasurement
