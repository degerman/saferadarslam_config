%make sure we are in correct folder
[f1,f2]=fileparts(pwd);
if strcmp(f2,getenv('SLAM_REPO'))
    %do nothing
elseif strcmp(f2,getenv('SLAM_CONFIG_REPO'))
    cd(fullfile(f1,getenv('SLAM_REPO')))
elseif ~isempty(dir(getenv('SLAM_REPO')))
    cd(getenv('SLAM_REPO'));
else
    error('go to the slam root folder');
end
clear all
close all

data_param_json    = '../saferadarslam_config/param/realtime/realtime_fusion.json';
sensor_param_json  = '../saferadarslam_config/param/realtime/multisensor_param.json';
slam_param_json    = '../saferadarslam_config/param/realtime/slam_param.json';

[~, ~, LidarParamLf, LidarParamRf, ~, ~, ~, TrkParam, ~] = i_o.TrackingParam;
[~, SS, ~] = i_o.GlobalParameters;
% runner = slam.i_o.Runner(data_param_json,sensor_param_json,slam_param_json);
% 
% runner.initFusion();
% 
% runner.initSensorConfiguration('allsensors');
% 
% runner.loadSensorData();
% 
% runner.setupSensorsFusion();

%% Set up udp port for lidars.
remoteIpLidarLf = '192.168.4.106';
remoteIpLidarRf = '192.168.4.108';
localIpPortLidarLf = 10000;
localIpPortLidarRf = 10001;

lidarLf = api.LidarReader(remoteIpLidarLf,localIpPortLidarLf,SS,LidarParamLf,TrkParam);
lidarRf = api.LidarReader(remoteIpLidarRf,localIpPortLidarRf,SS,LidarParamRf,TrkParam);

