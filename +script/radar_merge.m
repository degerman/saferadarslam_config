
%"file_names": [ "clustered_format_SLAM_20170823_009_all_in_one_constant_vel_rev_C.mat",

%concatenate

clear all

directory = '/Volumes/Elements/20170823/20170823_Ego_straight_in_intersection';

scenario = {'20170823_009','20170823_0010','20170823_0011','20170823_0012'};

for ind = 1:length(scenario)
    
    for BW_case = 0:2
        
        raw_data_dir = fullfile(directory,scenario{ind});
        
        files = dir([raw_data_dir,'_CPI*BW_case' num2str(BW_case) '.mat']);
        
        All2 = [];
        
        disp(['BW_case ' num2str(BW_case)]);
        
        for i=1:length(files)
            
            files(i).name;
            disp(['file ' files(i).name]);
            load(fullfile(files(i).folder,files(i).name));
            All2  = [All2,All];
            
        end
        
        All = All2;
        
        %gettime
        disp('getting UTC');
        f = dir(fullfile(raw_data_dir,'*.conf'));
        
        UTC_time = zeros(1,length(f));
        
        for i = 1:length(f)
            
            fid         = fopen(fullfile(raw_data_dir,[num2str(i),'.conf']),'r');
            C           = textscan(fid, '%s');
            UTC_time(i) = str2double(C{1}{19});
            fclose(fid);
            
        end
        
        target_directory = '/Users/johandegerman/data/20170823_save_airport/metal_poles_straight';
        
        target_file = fullfile(target_directory,[scenario{ind},'_BW_case' num2str(BW_case) '.mat'])
        
        save(target_file,'All','UTC_time');
    end
end