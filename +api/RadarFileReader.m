classdef RadarFileReader<slam.i_o.DataReader
    
    
    properties
        
        radar_detection_file
        
        AllPolar
        All
        EGO_time
        
    end
    
    methods
        
        function self = RadarFileReader(radar_detection_file,time_offset)
            
            if nargin==2
                self.UTC_time_offset = time_offset;
            else
                self.UTC_time_offset = 0;
            end
            
            self.radar_detection_file = radar_detection_file;
            
            %Get Radar measurements
            load(self.radar_detection_file);
            %CPI's are 1:230
            self.index = 1;
            self.N = length(clustered_data.AllPolar);
            
            self.AllPolar = clustered_data.AllPolar;
            self.All = clustered_data.All;
            self.EGO_time = clustered_data.EGO_time;
            
        end
                        
        function radar_meas = getNextMeasurement(self,sensor)
            
            if isempty(self.AllPolar{self.index})
                range   = [];
                u       = [];
                v       = [];
                doppler = [];
                SNR     = [];
            else
                range   = self.AllPolar{self.index}(:,1)';
                u       = -self.AllPolar{self.index}(:,2)'; %the classic minus sign
                v       = self.AllPolar{self.index}(:,3)';
                doppler = self.AllPolar{self.index}(:,4)';
%                  SNR     = 50*ones(size(range));
%                 SNR     = self.sensor.getSNR(1,range);
                SNR     = 2*cell2mat(self.All{self.index});
            end
            
            current_time = self.EGO_time{self.index} + ...
                           self.UTC_time_offset;
            radar_meas = slam.sensor.RadarMeasurement(current_time,range,u,v,doppler,SNR,[],sensor.param.sensor_id);
                        
            self.index  = self.index + 1;
                                    
        end
                
        function UTC_time = getUTCfromDirectory(self,raw_data_directory)
            
            %get UTC from radar log
            files = dir(fullfile(self.directory,raw_data_directory,'*.conf'));
            UTC_time = zeros(1,length(files));
            
            self.index = 1;
            self.N = length(files);
            
            for i = 1:length(files)
                
                fid         = fopen(fullfile(self.directory,raw_data_directory,[num2str(i),'.conf']),'r');
                C           = textscan(fid, '%s');
                UTC_time(i) = str2double(C{1}{19});
                fclose(fid);
                
            end
            
        end        
                
    end   
    
end

