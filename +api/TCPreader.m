classdef TCPreader<slam.i_o.DataReader

properties

  port
  client

end
    
methods

  function self = TCPreader(port)
  
    if nargin==0
  
  self.port = 3001;
    else
	self.port = port;
    end

    try
      self.client = tcpclient('localhost',self.port);
    catch
      disp(['no connection established on ' num2str(self.port)]);
      self.client = [];
    end
    self.index = 1;
    self.N = 1000;

  end

  function dets = getNextMeasurement(self,sensor)

    time  = posixtime(datetime('now'));
    range = [];
    doppler = [];
    u     = [];
    v     = [];
    SNR   = [];
  
    self.client.write(uint8(1));

    if self.client.BytesAvailable > 0

      try
    
        a = self.client.read();

        b = char(a);

        scan = jsondecode(b);

        tmp = cell2mat(struct2cell(scan.detections));

        time  = posixtime(datetime('now'))-3600;
 %       time  = scan.time;
        range = tmp(1,:);
        doppler = tmp(2,:);
        u     = -tmp(3,:);
        v     = tmp(4,:);
        SNR   = 2*tmp(5,:);
SNR(SNR>200) = 200;

      end

    end

    dets = slam.sensor.RadarMeasurement(time,range,u,v,doppler,SNR,[],sensor.param.sensor_id);

  end
end

end

