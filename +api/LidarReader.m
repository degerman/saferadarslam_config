classdef LidarReader<i_o.DataReader

    
    properties
        remoteIp
        localIpPort
        LidarParam
        TrkParam
        udpRx
        SS
    end
    
    methods
        
        function self = LidarReader(remoteIp,localIpPort,SS,LidarParam,TrkParam)
            
            self.index  = 1;
            self.N      = 1e9;
            % For left lidar: 
            % - remoteIp = '192.168.4.106';
            % - localIpPort '10000';
            % For right lidar: 
            % - remoteIp = '192.168.4.108';
            % - localIpPort '10000';            
            self.remoteIp     = remoteIp;
            self.localIpPort  = localIpPort;
            self.SS           = SS;
            self.LidarParam   = LidarParam;
            self.TrkParam     = TrkParam;
            udpRx = dsp.UDPReceiver('MessageDataType','uint8');
            udpRx.RemoteIPAddress      = self.remoteIp;
            udpRx.LocalIPPort          = self.localIpPort;
            udpRx.ReceiveBufferSize    = 48162;
            udpRx.MaximumMessageLength = 48162;
            setup(udpRx);
            self.udpRx = udpRx;
        end
        
        function meas = getNextMeasurement(self,sensor)
            locData = 1;
            data = self.udpRx();
            drawnow;
%            jsondecode(msg_meta)
            if isempty(data)
                meas = 'empty';
            else
                LidarData = i_o.unpackLidarUdp_mex(data,self.SS,self.LidarParam);
                [range,azimuth,elevation,covSpher,layer,SNR,delay] = ...
                    i_o.unpkLidarData_mex(LidarData,self.LidarParam,self.SS);
                if locData
                    % Convert data to local cartesian coordinates in sensor relative frame.
                    [xRel,yRel,zRel,covRel] =  i_o.spher2cart_mex(range,azimuth,elevation,covSpher,self.SS);
                    % Remove non-valuable detections by clustering.
                    [~,~,~,~,~,layerClus,SNRclus,delayClus] = ...
                        i_o.clusteringLidar_mex(xRel,yRel,zRel,covRel,layer,SNR,delay,self.LidarParam,self.SS,self.TrkParam);
                    valIdx = self.LidarParam.NdetLayer*(double(layerClus)-1)+(1:self.LidarParam.NdetLayer)';
                    valIdx = valIdx(layerClus~=self.SS.u1_Invalid);
                else
                    valIdx = zeros(1,0,'double');
                end
                %                 meas.time     = LidarData.fl_CurrTime - delay;
                time     = LidarData.fl_CurrTime - delay(1) - sensor.param.lag;
                az       = double(azimuth)+sensor.param.az_offset;
                        el       = double(elevation)+sensor.param.el_offset;
                u        = sin(az).*cos(el);
                v        = sin(el);

                meas  = slam.sensor.LidarMeasurement(double(time),double(range),az,el,u,v,...
                    double(SNR),valIdx,sensor.param.sensor_id);
            end
        end
        
    end
end

