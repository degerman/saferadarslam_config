classdef GridMapSender<handle
    
    
    properties
        
        jar_file
        adress
        
        ctx
        socket1
        
    end
    
    methods
        
        function self = GridMapSender(jar_file,adress)
            
            self.jar_file = jar_file;
            self.adress = adress;
            
            javaclasspath(jar_file);
            
            import org.zeromq.*
            
            %set socket
            self.ctx     = zmq.Ctx();
            
            self.socket1  = self.ctx.createSocket(ZMQ.PUB);

            self.socket1.bind(adress);
            
        end
        
        function send_ego(self,ego)
            
            OGM = [];
            OGM2D = [];
            
            Ego.X = ego.pos(1);
            Ego.Y = ego.pos(2);
            Ego.Z = ego.pos(3);
            Ego.yaw = ego.angle(1);
            Ego.pitch = ego.angle(2);
            Ego.roll = ego.angle(3);
            Ego.time = ego.time;
            
            Local = [];
            
            Meas = [];
            
            codedMessage = i_o.ksonPackCellStruct({OGM,OGM2D,Ego,Local,Meas});
            
            msg = zmq.Msg(length(codedMessage));
            msg.put(codedMessage);
            self.socket1.send(msg,0);
            
        end
        
        function send_localization(self,particleFilter,ego)
            
            pos = particleFilter.getEstimate().pos;
            angle = particleFilter.getEstimate().angle;
            
            OGM = [];
            OGM2D = [];
            
            Ego.X = ego.pos(1);
            Ego.Y = ego.pos(2);
            Ego.Z = ego.pos(3);
            Ego.yaw = ego.angle(1);
            Ego.pitch = ego.angle(2);
            Ego.roll = ego.angle(3);
            Ego.time = ego.time;
            
            Local.x = pos(1);
            Local.y = pos(2);
            Local.z = pos(3)*0;
            Local.yaw = angle(1);
            Local.pitch = angle(2);
            Local.roll = angle(3);
            particle_position = zeros(3,particleFilter.nrParticles);
            particle_weight = zeros(1,particleFilter.nrParticles);
            
            for i=1:particleFilter.nrParticles
                [pos, ~, ~, ~, w] = particleFilter.getParticleState(i);
                particle_position(:,i) = pos';
                particle_weight(i) = w;
            end
            
            Local.xp = particle_position(1,:);
            Local.yp = particle_position(2,:);
            Local.zp = particle_position(3,:);
            Local.Pd = particle_weight;
            
            Meas = [];
            
            codedMessage = i_o.ksonPackCellStruct({OGM,OGM2D,Ego,Local,Meas});
            
            msg = zmq.Msg(length(codedMessage));
            msg.put(codedMessage);
            self.socket1.send(msg,0);
            
        end
        
        function send_ogm(self,map,fullmap)
            if nargin > 2
                OGM = api.interface.getOccupancyGridList(map,1,0.01);
                OGM2D = api.interface.getOccupancyGrid2D(map,1);
            else
                OGM = api.interface.getOccupancyGridList(map,0);
                OGM2D = api.interface.getOccupancyGrid2D(map,0);
            end
            
            Ego = [];
            
            Local = [];
            
            Meas = [];
            
            codedMessage = i_o.ksonPackCellStruct({OGM,OGM2D,Ego,Local,Meas});
            
            msg = zmq.Msg(length(codedMessage));
            msg.put(codedMessage);
            self.socket1.send(msg,0);
            
        end
        
        function send(self,map,ego)
            
            OGM = api.interface.getOccupancyGridList(map,0);
            OGM2D = api.interface.getOccupancyGrid2D(map,0);
            
            Ego.X = ego.pos(1);
            Ego.Y = ego.pos(2);
            Ego.Z = ego.pos(3);
            Ego.yaw = ego.angle(1);
            Ego.pitch = ego.angle(2);
            Ego.roll = ego.angle(3);
            Ego.time = ego.time;
            
            Local.x = ((Ego.X) + (Ego.X+5-(Ego.X))*rand(1000,1))*cos(Ego.yaw)+((Ego.Y) + (Ego.Y+2-(Ego.Y))*rand(1000,1))*sin(Ego.yaw);
            Local.y = ((Ego.Y) + (Ego.Y+2-(Ego.Y))*rand(1000,1))*cos(Ego.yaw)+((Ego.X) + (Ego.X+5-(Ego.X))*rand(1000,1))*sin(Ego.yaw);
            Local.z = (Ego.Z) + (Ego.Z+1.5-(Ego.Z))*rand(1000,1);
            Local.Pd = rand(1000,1);
            
            %             Requires Stat-tool box
            %             Local.x = normrnd(Ego.X,5,[1,100]);
            %             Local.y = normrnd(Ego.Y,2,[1,100]);
            %             Local.z = normrnd(Ego.Z,1.5,[1,100]);
            %             Local.Pd = normrnd(0,1,[1,100]);
            
            Meas = [];
            
            codedMessage = i_o.ksonPackCellStruct({OGM,OGM2D,Ego,Local,Meas});
            
            msg = zmq.Msg(length(codedMessage));
            msg.put(codedMessage);
            self.socket1.send(msg,0);
            
        end
        
        function send_measurements(self,data)
            
            %            ind = data(3,:) < 2 & data(3,:) > -1;
            OGM = [];
            
            OGM2D = [];
            
            Ego = [];
            
            Local = [];
            
            Meas.x = data(1,:);
            Meas.y = data(2,:);
            Meas.z = data(3,:);
            Meas.Pd = 0.25*ones(1,length(Meas.x));
            
            codedMessage = i_o.ksonPackCellStruct({OGM,OGM2D,Ego,Local,Meas});
            
            msg = zmq.Msg(length(codedMessage));
            msg.put(codedMessage);
            self.socket1.send(msg,0);
            
        end
        
        function close(self)
            
            self.socket1.close();
            
        end
        
    end
end

