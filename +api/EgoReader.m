classdef EgoReader<i_o.DataReader
    
    
    properties
        
        jar_file
        adress
        
        ctx
        socket1
        
        lon_anchor
        lat_anchor
        alt_anchor
        
        angle_bias
    end
    
    methods
        
        function self = EgoReader(jar_file,adress, angle_bias, lon_anchor, lat_anchor, alt_anchor)
            
            self.jar_file = jar_file;%'/home/saferadar/git/jeromq-master/target/jeromq-0.4.4-SNAPSHOT.jar'; % Set this to the correct jar path
            self.adress = adress;%'tcp://192.168.4.107:3344'; % very much hard-coded
            
            javaclasspath(self.jar_file);
            
            import org.zeromq.*
            
            %set pub socket for subscibe
            self.ctx     = ZMQ.context(1);
            
            self.socket1  = self.ctx.socket(ZMQ.SUB);
            
            self.socket1.setConflate(1);
            self.socket1.setRcvHWM(1);
            
            self.socket1.connect(self.adress);
            self.socket1.subscribe('');
            self.socket1.setReceiveTimeOut(1e1);
            
            if nargin > 2
                self.angle_bias = angle_bias;
            else
                self.angle_bias = 0;
            end
            
            if nargin < 4
                self.lon_anchor = 0;
                self.lat_anchor = 0;
                self.alt_anchor = 0;
            else
                self.lon_anchor = lon_anchor;
                self.lat_anchor = lat_anchor;
                self.alt_anchor = alt_anchor;
            end
            
            %max number of measurements
            self.N = 1e9;
            
        end
        
        function data = flush(self)
            data = self.socket1.recv(1);
            for i=1:1000
                data_new = self.socket1.recv(1);
                if isempty(data_new)
                    break;
                end
                data = data_new;
            end
        end

        function meas = getNextMeasurement(self,sensor)
            no_meas = true;
            i = 1;
            while no_meas && i<100
            try
                egodata = jsondecode(native2unicode(self.socket1.recv(1)'));
                
                if isempty(egodata)
                    meas = 'empty';
                else
                    [x, y, z, speed] = self.getPositionFromRef(egodata);
                    [yaw, pitch, roll] = self.getOrientationFromRef(egodata);
                    yaw     = yaw - sensor.param.angle_bias(1);
                    pitch   = pitch - sensor.param.angle_bias(2);
                    roll    = roll - sensor.param.angle_bias(3);
                    
                    
                    ang_rate = [egodata.AngRateZ egodata.AngRateY egodata.AngRateX]'./180.*pi.*(-1);
                    
                    %sensor pos in cartesian = ego pos in cartesian +
                    %R*sensor pos in local
                    %ego pos in cartesian = sensor pos in cartesian -
                    %R*sensor pos in local

%                    R           = slam.sensor.Sensor.rotationMatrix3D(yaw,pitch,roll);
%                    sensor_pos  = [0;0;0] + R*sensor.param.pos;

                    ego_meas    = slam.localization.EgoState([x y z]',[yaw pitch roll]',...
                        speed,ang_rate);
                    
                    ego_meas.time = egodata.Time;
                    
                    self.index  = self.index + 1;
                    meas = ego_meas;
                end
                    no_meas = false;
            catch
                meas = 'empty';
	end
                i = i + 1;
            end
        end
        
        function [x, y, z, speed, time, x_t, y_t, z_t, speed_t] = getPositionFromRef(self,DATA)
            EARTH_RADIUS = 6371e3;
            
            %either it is in in EGO or Target struct
            if DATA.EgoValid
                lon     = DATA.PosLon';
                lat     = DATA.PosLat';
                alt     = DATA.PosAlt';
                speed   = DATA.Speed2D';
                
                dlat    = lat - self.lat_anchor;
                dlon    = lon - self.lon_anchor;
                dx      = sind(dlon).*cosd(lat)*EARTH_RADIUS;
                dy      = sind(dlat)*EARTH_RADIUS;
                dr      = sqrt(dx.*dx+dy.*dy);
                a       = atan2(dy,dx);
                x       = dr.*cos(a+(0-pi/2));
                y       = dr.*sin(a+(0-pi/2));
                
  %             z       = alt-alt(1);
                z       = alt-self.alt_anchor;
                
            else
                x       = 0;
                y       = 0;
                z       = 0;
                speed   = 0;
            end
            if DATA.TargetValid
                lon_t     = DATA.TargetPosLon';
                lat_t     = DATA.TargetPosLat';
                alt_t     = DATA.TargetPosAlt';
                speed_t   = DATA.TargetSpeed3D';
                dlat    = lat_t - self.lat_anchor;
                dlon    = lon_t - self.lon_anchor;
                dx      = sind(dlon).*cosd(lat_t)*EARTH_RADIUS;
                dy      = sind(dlat)*EARTH_RADIUS;
                dr      = sqrt(dx.*dx+dy.*dy);
                a       = atan2(dy,dx);
                x_t       = dr.*cos(a+(0-pi/2));
                y_t       = dr.*sin(a+(0-pi/2));
                z_t       = alt_t-alt_t(1);
            else
                x_t       = [];
                y_t       = [];
                z_t       = [];
                speed_t   = [];
            end
            time    = DATA.Time';
        end
        
        function xyz = getPositionFromPole(self, pole)
            data = struct();
            data.PosLon = pole.Lon;
            data.PosLat = pole.Lat;
            n = size(pole.Lon,1);
            data.PosAlt = zeros(n,1);
            data.Speed2D = zeros(n,1);
            data.EgoValid = true;
            data.TargetValid = false;
            data.Time = 0;
            [x, y, z] = self.getPositionFromRef(data);
            xyz = [x;y;z];
        end
            
    end
    
    methods(Static)
        
        function [yaw, pitch, roll, yaw_t, pitch_t, roll_t] = getOrientationFromRef(DATA)
            
            % HEADING_BIAS = 1;%2;
            % PITCH_BIAS = 1.5;
            % ROLL_BIAS = 3;
            if DATA.EgoValid
                yaw      = 2*pi+(DATA.AngleHeading)'/180*pi*(-1);
                pitch    = (DATA.AnglePitch)'/180*pi*(-1);
                roll     = (DATA.AngleRoll)'/180*pi;
            else
                yaw     = 0;
                pitch   = 0;
                roll    = 0;
            end
            
            if DATA.TargetValid
                yaw_t      = DATA.TargetAngleHeading'/180*pi*(-1);
                pitch_t    = DATA.TargetAnglePitch'/180*pi*(-1);
                roll_t     = DATA.TargetAngleRoll'/180*pi;
                
            else
                yaw_t     = [];
                pitch_t   = [];
                roll_t    = [];
            end
            
        end
    end
end

