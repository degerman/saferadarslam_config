classdef EgoFileReader2test < matlab.unittest.TestCase
    
    methods(Test)

        function testScenario(self)
            close all
            reader = api.EgoFileReader2('/Users/pontus/Documents/data/SLAM/ego/ego_16',0);
            
            param = struct('Q_linear', struct(  'yaw_rate'  ,deg2rad(1),...
                                                'pitch_rate'  ,deg2rad(1),...
                                                'roll_rate'  ,deg2rad(1),...
                                                'speed'     ,0.3),...
                            'R_linear', struct( 'yaw_rate'  ,deg2rad(0.15),...
                                                'pitch_rate'  ,deg2rad(0.15),...
                                                'roll_rate'  ,deg2rad(0.15),...
                                                'speed'     ,0.007),...
                          'Q_non_linear',struct('x'         ,sqrt(0.001),...
                                                'y'         ,sqrt(0.001),...
                                                'z'         ,sqrt(0.001),...
                                                'yaw'       ,deg2rad(0.001),...
                                                'pitch'     ,deg2rad(0.001),...
                                                'roll'      ,deg2rad(0.001)),...
                            'C',                            eye(2),...
                            'Al',                           eye(2));
                        
            motionModel = slam.localization.FullMotionModel(param);
            
            pos = zeros(3,reader.N);
            angle = zeros(3,reader.N);
            angle_rate = zeros(3,reader.N);
            speed = zeros(1,reader.N);
            
            nonlinear = [];
            
            for i = 1:reader.N
                meas = reader.getNextMeasurement();
                
                pos(:,i) = meas.pos;
                angle(:,i) = meas.angle;
                angle_rate(:,i) = meas.angle_rate;
                speed(i) = meas.speed;
                if i == 1
                    nonlinear(:,1)  = [meas.pos;meas.angle];
                    time = meas.time;
                else
                    if meas.time - time > 0
                        nonlinear = [nonlinear nonlinear(:,end) + motionModel.An(nonlinear(:,end), meas.time - time )*[meas.angle_rate;meas.speed]];
%                         nonlinear(4:6,end) = unwrap(nonlinear(4:6,end));
%                         meas.time - time
                        time = meas.time;
                    end
                end
                
            end

            
            %%
            plot3(pos(1,:), pos(2,:), pos(3,:));
            hold on
            plot3(nonlinear(1,:), nonlinear(2,:), nonlinear(3,:));
            
            s = scatter3(pos(1,1), pos(2,1), pos(3,1));
            hold off
            axis equal
            
            
        end
    end 
end