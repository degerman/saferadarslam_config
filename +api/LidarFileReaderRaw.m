classdef LidarFileReaderRaw<i_o.DataReader
    %FileReader This loads data and pushes measurements to the system
    %   A first temporary FileReader is implmemented
    
    properties
        
        detParam
        
        au2_Range
        au2_Horz
        au1_Layer
        au1_SNR
        fl_CurrTime
    end
    
    methods

        function self = LidarFileReaderRaw(file_name,time_offset)
            
            if nargin==2
                self.UTC_time_offset = time_offset;
            else
                self.UTC_time_offset = 0;
            end
            
            tmp = load(file_name);
            self.au2_Range = tmp.lidarData.au2_Range;
            if isfield(tmp.lidarData,'au2_Horz')
                self.au2_Horz = tmp.lidarData.au2_Horz;
            else
               % fix this 
            end
            if isfield(tmp.lidarData,'au1_Layer')
                self.au1_Layer = tmp.lidarData.au1_Layer;
            else
                %fix this
            end
            self.au1_SNR = tmp.lidarData.au1_SNR;
            self.fl_CurrTime = tmp.lidarData.fl_CurrTime;
                        
            self.index  = 1;
            self.N      = length(self.fl_CurrTime.Time);
            
            % Lidar adapter values
            self.detParam.RngStd   = 0.1;
            self.detParam.RngStep  = 0.01;
            self.detParam.AzimStd  = (0.1*pi/180);
            self.detParam.ElevStd  = (0.15*pi/180);
            self.detParam.AzimStep = (0.15*pi/180);
            self.detParam.AzimOffs = (-60*pi/180);
            self.detParam.ElevStep = -(0.4*pi/180);
            self.detParam.ElevOffs = (1.4*pi/180);
                       
        end
                
        function lidar_meas = getNextMeasurement(self,sensor)
            
            %remove invaled and measurements with range shorter than 20 cm
            valid       = self.au2_Range.Data(self.index,:) ~= 65535 & ...
                          self.au2_Range.Data(self.index,:) > 20;
            % Get sensor data in speherical coordinates given data package format
            time        = double(self.fl_CurrTime.Time(self.index)) + ...
                          self.UTC_time_offset;
            range       = self.detParam.RngStep*double(self.au2_Range.Data(self.index,valid));
            azimuth     = self.detParam.AzimStep*double(self.au2_Horz.Data(self.index,valid)-1)+self.detParam.AzimOffs;
            elevation   = self.detParam.ElevStep*double(self.au1_Layer.Data(self.index,valid)-1)+self.detParam.ElevOffs;
            SNR         = double(self.au1_SNR.Data(self.index,valid))-25;
            
            lidar_meas  = slam.sensor.LidarMeasurement(time,range,-azimuth,elevation,SNR,sensor.param.sensor_id);
            
            self.index  = self.index + 1;
            
        end
        
    end
    
end

