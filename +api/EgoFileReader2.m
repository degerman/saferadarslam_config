classdef EgoFileReader2 < i_o.DataReader
    
    
    properties
        data_dir
        names
        lon_anchor
        lat_anchor
        angle_bias
    end
    
    methods
        
        function self = EgoFileReader2(data_dir, angle_bias, lon_anchor, lat_anchor)
            
            self.data_dir = data_dir;

            d = dir(data_dir);
            self.names = sort(str2double({d.name}));
            self.names = self.names( ~isnan(self.names) );
            
            self.index  = 1;
            self.N      = numel(self.names);
            f = fopen(strcat(self.data_dir , filesep,  string(self.names(self.index))));
            egodata = jsondecode(fread(f, '*char'));
            fclose(f);
            
            if nargin < 3
                self.lon_anchor = egodata.PosLon;
                self.lat_anchor = egodata.PosLat;
            else
                self.lon_anchor = lon_anchor;
                self.lat_anchor = lat_anchor;
            end
            
            self.angle_bias = angle_bias;
            
            
        end
                                
                
        
        function [ego_meas] = getNextMeasurement(self)

            f = fopen(strcat(self.data_dir , filesep,  string(self.names(self.index))));
            egodata = jsondecode(fread(f, '*char'));
            fclose(f);          
            
            [x, y, z, speed] = self.getPositionFromRef(egodata);
            [yaw, pitch, roll] = self.getOrientationFromRef(egodata);
            
            ang_rate = [egodata.AngRateZ egodata.AngRateY egodata.AngRateX]'./180.*pi.*(-1);

            ego_meas = slam.localization.EgoState([x y z]',[yaw pitch roll]',...
                                                  speed,ang_rate);
            
            ego_meas.time = egodata.GpsTime;
             
            
            self.index  = self.index + 1;
            
        end
        
        function [x, y, z, speed, time, x_t, y_t, z_t, speed_t] = getPositionFromRef(self,DATA)
             EARTH_RADIUS = 6371e3;
            
            %either it is in in EGO or Target struct
            if DATA.EgoValid
                lon     = DATA.PosLon';
                lat     = DATA.PosLat';
                alt     = DATA.PosAlt';
                speed   = DATA.Speed2D';
                
                dlat    = lat - self.lat_anchor;
                dlon    = lon - self.lon_anchor;
                dx      = sind(dlon).*cosd(lat)*EARTH_RADIUS;
                dy      = sind(dlat)*EARTH_RADIUS;
                dr      = sqrt(dx.*dx+dy.*dy);
                a       = atan2(dy,dx);
                x       = dr.*cos(a+(0-pi/2));
                y       = dr.*sin(a+(0-pi/2));

                z       = alt-alt(1);
            else
                x       = [];
                y       = [];
                z       = [];
                speed   = [];
            end
            if DATA.TargetValid
                lon_t     = DATA.TargetPosLon';
                lat_t     = DATA.TargetPosLat';
                alt_t     = DATA.TargetPosAlt';
                speed_t   = DATA.TargetSpeed3D';
                dlat    = lat_t - self.lat_anchor;
                dlon    = lon_t - self.lon_anchor;
                dx      = sind(dlon).*cosd(lat_t)*EARTH_RADIUS;
                dy      = sind(dlat)*EARTH_RADIUS;
                dr      = sqrt(dx.*dx+dy.*dy);
                a       = atan2(dy,dx);
                x_t       = dr.*cos(a+(0-pi/2));
                y_t       = dr.*sin(a+(0-pi/2));

                z_t       = alt_t-alt_t(1);
            else
                x_t       = [];
                y_t       = [];
                z_t       = [];
                speed_t   = [];
            end
            time    = DATA.Time';  
        end

    end
    
    methods(Static) 
                    
        function [yaw, pitch, roll, yaw_t, pitch_t, roll_t] = getOrientationFromRef(DATA)
            
           % HEADING_BIAS = 1;%2;
           % PITCH_BIAS = 1.5;
           % ROLL_BIAS = 3;
            if DATA.EgoValid
                yaw      = (DATA.AngleHeading)'/180*pi*(-1);
                pitch    = (DATA.AnglePitch)'/180*pi*(-1);
                roll     = (DATA.AngleRoll)'/180*pi;
            else
                yaw     = [];
                pitch   = [];
                roll    = [];
            end
            
            if DATA.TargetValid
                yaw_t      = DATA.TargetAngleHeading'/180*pi*(-1);
                pitch_t    = DATA.TargetAnglePitch'/180*pi*(-1);
                roll_t     = DATA.TargetAngleRoll'/180*pi;
                
            else
                yaw_t     = [];
                pitch_t   = [];
                roll_t    = [];
            end

        end   
        
    end
    
end

