classdef LidarFileReader<slam.i_o.DataReader
    %FileReader This loads data and pushes measurements to the system
    %   A first temporary FileReader is implmemented
    
    properties
                
        refData
        egoData
        
    end
    
    methods
        

        function self = LidarFileReader(file_name)
                        
            load(file_name);
            
            self.sensorData = sensorData;
            self.refData    = refData;
            self.egoData    = egoData;
            
            self.index      = 1;
            
            self.N          = length(self.sensorData.fl_CurrTime.Data);
            
        end
     
        
%         function self = LidarFileReader()
%             
%             self.sensor = slam.grid.LidarSensor();
%             
%             %hard-code the lidar data
%             self.directory   = '/Users/johandegerman/data/denso/';
%             %            directory   ='/home/johan/data/AstaZero_April_2017_Lidar/slam.localization_around_the_block';
%             data_files  = dir(fullfile(self.directory,'*.mat'));
%             
%             nr = 1;
%             load(fullfile(self.directory,data_files(nr).name));
%             
%             self.sensorData = sensorData;
%             self.refData    = refData;
%             self.egoData    = egoData;
%             
%             self.index      = 1;
%             
%             self.N          = length(self.sensorData.fl_CurrTime.Data);
%             
%         end
        
        
        function pos = getTrajectory(self)
            
            pos  = [self.refData.db_EgoPosX.Data';
                self.refData.db_EgoPosY.Data';
                self.refData.db_EgoPosZ.Data'];
            
        end
        
        
        function limits = getLimits(self)
            
            margin      = 10;
            pos         = self.getTrajectory();
            limits      = round([min(pos(1,:)),max(pos(1,:)),min(pos(2,:)),max(pos(2,:)),-2,3]);
            limits      = limits + [-margin,margin,-margin,margin,0,0];
            limits(2:2:4) = ceil(limits(2:2:4)/10)*10;
            limits(1:2:3) = floor(limits(1:2:3)/10)*10;
        end

                
        function [lidar_meas, ego_meas] = getNextScan(self)
            
            %the sensor data
            time        = self.sensorData.fl_CurrTime.Data(self.index);
            range       = self.sensorData.afl_Range.Data(self.index,:);
            azimuth     = self.sensorData.afl_Azimuth.Data(self.index,:);
            elevation   = self.sensorData.afl_Elevation.Data(self.index,:);
            SNR         = self.sensorData.afl_SNR.Data(self.index,:);
            
            self.index = self.index + 1;
            
            %reference system data interpolated to sensor time
            pos  = [interp1(self.refData.db_EgoPosX.Time,self.refData.db_EgoPosX.Data,time);
                interp1(self.refData.db_EgoPosY.Time,self.refData.db_EgoPosY.Data,time);
                interp1(self.refData.db_EgoPosZ.Time,self.refData.db_EgoPosZ.Data,time)];
            
            ang  = [interp1(self.refData.db_EgoCourse.Time,self.refData.db_EgoCourse.Data,time);
                interp1(self.refData.db_EgoPitch.Time,self.refData.db_EgoPitch.Data,time);
                interp1(self.refData.db_EgoRoll.Time,self.refData.db_EgoRoll.Data,time)];
            
            speed =  interp1(self.refData.db_EgoSpeed.Time,self.refData.db_EgoSpeed.Data,time);
            ego_vel = self.sensor.rotationMatrix3D(ang(1),ang(2),ang(3))*[speed;0;0];
            
            
            ang_rate = [interp1(self.refData.db_EgoYawRate.Time,self.refData.db_EgoYawRate.Data,time);
                interp1(self.refData.db_EgoPitchRate.Time,self.refData.db_EgoPitchRate.Data,time);
                interp1(self.refData.db_EgoRollRate.Time,self.refData.db_EgoRollRate.Data,time)];
            
            %vehicle data
            speed2      = interp1(self.egoData.fl_Speed.Time,self.egoData.fl_Speed.Data,time);
            yawrate2    = interp1(self.egoData.fl_YawRate.Time,self.egoData.fl_YawRate.Data,time);
            
%             scan        = struct('UTC_time',[],'ego_pos',pos,'ego_angle',ang,...
%                 'ego_speed',speed,'ego_vel',ego_vel,'ego_angle_rate',ang_rate,...
%                 'range',range,'azimuth',azimuth,'elevation',elevation,'SNR',SNR,...
%                 'R',[],'LM_id',[],'LM_pos',[],'t',time);
            
            %             target_pos= [interp1(self.refData.db_ObjPosX.Time,self.refData.db_ObjPosX.Data,time_vec);
            %                         interp1(self.refData.db_ObjPosY.Time,self.refData.db_ObjPosY.Data,time_vec);
            %                         interp1(self.refData.db_ObjPosZ.Time,self.refData.db_ObjPosZ.Data,time_vec)];
            %             target_ang= [interp1(self.refData.db_ObjCourse.Time,self.refData.db_ObjCourse.Data,time_vec);
            %                         interp1(self.refData.db_ObjPitch.Time,self.refData.db_ObjPitch.Data,time_vec);
            %                         interp1(self.refData.db_ObjRoll.Time,self.refData.db_ObjRoll.Data,time_vec)];
            
            %            UTC_time = interp1(sync.time,sync.UTC_time,time_vec);

            ego_meas = slam.localization.EgoState(pos,ang,speed,ang_rate);
            lidar_meas = slam.sensor.LidarMeasurement(range,azimuth,elevation,SNR);
            
        end
        
        function scan = getNextScanEgo(self,no_increment)
            if(nargin < 2)
                no_increment = 0;
            end
            
            time        = self.sensorData.fl_CurrTime.Data(self.index);
            range       = self.sensorData.afl_Range.Data(self.index,:);
            idx = range<1000;
            range = range(idx);
            azimuth     = self.sensorData.afl_Azimuth.Data(self.index,:);
            azimuth = azimuth(idx);
            elevation   = self.sensorData.afl_Elevation.Data(self.index,:);
            elevation = elevation(idx);
            SNR         = self.sensorData.afl_SNR.Data(self.index,:);
            SNR = SNR(idx);
            
            if(~no_increment)
                self.index = self.index + 1;
            end
            
            % reference data
            ang_rate = [interp1(self.refData.db_EgoYawRate.Time,self.refData.db_EgoYawRate.Data,time);
                        interp1(self.refData.db_EgoPitchRate.Time,self.refData.db_EgoPitchRate.Data,time);
                        interp1(self.refData.db_EgoRollRate.Time,self.refData.db_EgoRollRate.Data,time)];
           
            speed =  interp1(self.refData.db_EgoSpeed.Time,self.refData.db_EgoSpeed.Data,time);
            %vehicle data
            %speed2        = interp1(self.egoData.fl_Speed.Time,self.egoData.fl_Speed.Data,time);
            %ang_rate(3,:) = interp1(self.egoData.fl_YawRate.Time,self.egoData.fl_YawRate.Data,time);
            
            scan        = struct('UTC_time',[],...
                                 'ego_speed',speed,'ego_angle_rate',ang_rate,...
                                 'range',range,'azimuth',azimuth,'elevation',elevation,'SNR',SNR,...
                                 'R',[],'LM_id',[],'LM_pos',[],'t',time);
            
        end

        
    end
    
end

