classdef EgoFileReader<i_o.DataReader
    
    
    properties
        ground_truth_file
        UTC_time
        pos
        angle
        angle_rate
        speed
        pole
        lon_anchor
        lat_anchor
    end
    
    methods
        
        function self = EgoFileReader(ground_truth_file, angle_bias, lon_anchor, lat_anchor)
            
            self.ground_truth_file = ground_truth_file;

            load(self.ground_truth_file);
            
            if(nargin<3)
                self.lon_anchor  = RT2002.LatitudeLongitude.PosLon(1);
                self.lat_anchor  = RT2002.LatitudeLongitude.PosLat(1);
            else
                self.lon_anchor = lon_anchor;
                self.lat_anchor = lat_anchor;
            end
            
            self.UTC_time = self.getUTCtimeFromRef(RT2002);
            [x,y,z,self.speed] = self.getPositionFromRef(RT2002);
            [yaw,pitch,roll] = self.getOrientationFromRef(RT2002);

            if length(x)<length(self.UTC_time)
                N = length(x);
                self.UTC_time = self.UTC_time(1:N);
                yaw     = yaw(1:N);
                pitch   = pitch(1:N);
                roll    = roll(1:N);
                self.speed = self.speed(1:N);

            end
            if length(self.speed)>length(self.UTC_time)
                N   = length(self.UTC_time);
                x   = x(1:N);
                y   = y(1:N);
                z   = z(1:N);
                self.speed = self.speed(1:N);
                yaw     = yaw(1:N);
                pitch   = pitch(1:N);
                roll    = roll(1:N);
            end
                
            yawRate     = gradient(yaw)./gradient(self.UTC_time)';
            pitchRate   = gradient(pitch)./gradient(self.UTC_time)';
            rollRate    = gradient(roll)./gradient(self.UTC_time)';
            
            self.pos    = [x;y;z];
            self.angle  = [yaw;pitch;roll] - repmat(angle_bias(:),1,length(yaw));
            self.angle_rate = [yawRate;pitchRate;rollRate];
            
            self.index  = 1;
            self.N      = length(self.UTC_time);

        end
                                
        function traj = getTrajectory(self)
            
            traj  = self.pos;
        end
                
        function limits = getLimits(self,margin)

            limits      = round([min(self.pos(1,:)),max(self.pos(1,:)),min(self.pos(2,:)),max(self.pos(2,:)),-2,3]);
            limits      = limits + [-margin,margin,-margin,margin,0,0];
            limits(2:2:4) = ceil(limits(2:2:4)/10)*10;
            limits(1:2:3) = floor(limits(1:2:3)/10)*10;
        end
        
        function [ego_meas] = getNextMeasurement(self,sensor)
                        
            time = self.UTC_time(self.index);
            
            ego_meas = slam.localization.EgoState(self.pos(:,self.index),self.angle(:,self.index),...
                                                  self.speed(self.index),self.angle_rate(:,self.index));
            ego_meas.time = time;
            
%            ego_meas = slam.sensor.EgoMeasurement(time,self.pos(:,self.index),self.angle(:,self.index),...
%                                                  self.angle_rate(:,self.index),self.speed(self.index),sensor.index);
            
            self.index  = self.index + 1;
            
        end

        function [ego_meas] = getMeasurementSync(self,sensor_UTC_time,offset)
            
            if nargin == 2
                offset = 0;
            end
            
            sensor_time = sensor_UTC_time + offset;
            
            pos_  = [interp1(self.UTC_time,self.pos(1,:),sensor_time);
                interp1(self.UTC_time,self.pos(2,:),sensor_time);
                interp1(self.UTC_time,self.pos(3,:),sensor_time)];
            
            angle_  = [interp1(self.UTC_time,self.angle(1,:),sensor_time);
                interp1(self.UTC_time,self.angle(2,:),sensor_time);
                interp1(self.UTC_time,self.angle(3,:),sensor_time)];
            
            angle_rate_  = [interp1(self.UTC_time,self.angle_rate(1,:),sensor_time);
                interp1(self.UTC_time,self.angle_rate(2,:),sensor_time);
                interp1(self.UTC_time,self.angle_rate(3,:),sensor_time)];
            
            speed_ = interp1(self.UTC_time,self.speed,sensor_time);
            
            ego_meas = slam.localization.EgoState(pos_,angle_,speed_,angle_rate_);
            
        end
            
        function [x,y,z,speed,time] = getPositionFromRef(self,DATA)
            
            EARTH_RADIUS = 6371e3;
            
            %either it is in in EGO or Target struct
            if isfield(DATA,'LatitudeLongitude')
                lon     = DATA.LatitudeLongitude.PosLon';
                lat     = DATA.LatitudeLongitude.PosLat';
                alt     = DATA.Altitude.PosAlt';
                speed   = DATA.Velocity.Speed2D';
                time    = DATA.LatitudeLongitude.time';

            elseif isfield(DATA,'TargetLatitudeLongitude')
                lon     = DATA.TargetLatitudeLongitude.TargetPosLon';
                lat     = DATA.TargetLatitudeLongitude.TargetPosLat';
                alt     = DATA.TargetAltitude.TargetPosAlt';
                speed   = DATA.TargetVelocity.TargetSpeed3D';
                time    = DATA.TargetLatitudeLongitude.time';
            else
                error('wrong struct');
            end

            dlat    = lat - self.lat_anchor;
            dlon    = lon - self.lon_anchor;
            dx      = sind(dlon).*cosd(lat)*EARTH_RADIUS;
            dy      = sind(dlat)*EARTH_RADIUS;
            dr      = sqrt(dx.*dx+dy.*dy);
            a       = atan2(dy,dx);
            x       = dr.*cos(a+(0-pi/2));
            y       = dr.*sin(a+(0-pi/2));
            
            z       = alt-alt(1);
            
        end
    end    
    
    
    
    methods(Static)
        
        function UTC_time = getUTCtimeFromRef(DATA)
            
            GPS_LEAP_SECONDS = 19;
            
            DELAY = 0.05; %w.r.t radar
            DELAY = 0; %w.r.t radar
            
            UTC_time = DATA.DateTime.TimeMinute*60 + DATA.DateTime.TimeSecond + DATA.DateTime.TimeHSecond - GPS_LEAP_SECONDS - DELAY;
            
        end
                
                    
        function [yaw,pitch,roll] = getOrientationFromRef(DATA)
            
           % HEADING_BIAS = 1;%2;
           % PITCH_BIAS = 1.5;
           % ROLL_BIAS = 3;
            if isfield(DATA,'HeadingPitchRoll')
                yaw      = (DATA.HeadingPitchRoll.AngleHeading)'/180*pi*(-1);
                pitch    = (DATA.HeadingPitchRoll.AnglePitch)'/180*pi*(-1);
                roll     = (DATA.HeadingPitchRoll.AngleRoll)'/180*pi;
                
            elseif isfield(DATA,'TargetLatitudeLongitude')
                yaw      = DATA.TargetHeadingPitchRoll.TargetAngleHeading'/180*pi*(-1);
                pitch    = DATA.TargetHeadingPitchRoll.TargetAnglePitch'/180*pi*(-1);
                roll     = DATA.TargetHeadingPitchRoll.TargetAngleRoll'/180*pi;
                
            else
                yaw     = [];
                pitch   = [];
                roll    = [];
            end

        end   
        
    end
        
end

