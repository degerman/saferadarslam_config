function [Pd,x,y,z] = getFreeSpaceList(theGrid,th)

if nargin==1
    
    th = -0.01;    
end

Pd  = [];
x   = [];
y   = [];
z   = [];

for i = 1:size(theGrid.occupancyGrids,1)
    for j = 1:size(theGrid.occupancyGrids,2)
        
        if theGrid.occupancyGrids{i,j}.init==1
            
            ind     = find(theGrid.occupancyGrids{i,j}.L<th);
            siz     = size(theGrid.occupancyGrids{i,j}.L);
            [x_ind,y_ind,z_ind] = ind2sub(siz,ind);
            
            if ~isempty(x_ind)
                Pd  = [Pd,slam.sensor.Sensor.logOdds2Pd(theGrid.occupancyGrids{i,j}.L(ind)')];
                x   = [x,theGrid.occupancyGrids{i,j}.x(x_ind)];
                y   = [y,theGrid.occupancyGrids{i,j}.y(y_ind)];
                z   = [z,theGrid.occupancyGrids{i,j}.z(z_ind)];
            end
        end
    end

end
