function [Pd,x,y,z] = getOccupancyGrid2D(theGrid,fullMap)

if nargin == 1
    fullMap = 0;
end

if fullMap == 1
    
    Pd = 0.5*ones(length(theGrid.x)-1,length(theGrid.y)-1);
    x = theGrid.x(1:end-1);
    y = theGrid.y(1:end-1);
    z = theGrid.z(1:end-1);
    
    % get the indeces of each sub-grid
    x_ind = (theGrid.sub_x-theGrid.sub_x(1))/theGrid.param.dx + 1;
    y_ind = (theGrid.sub_y-theGrid.sub_y(1))/theGrid.param.dy + 1;
    
    for i = 1:size(theGrid.occupancyGrids,1)
        for j = 1:size(theGrid.occupancyGrids,2)
            if theGrid.occupancyGrids{i,j}.init==1
            
            %we need to compute the indeces in the theGrid for subgrid i
            Pd(x_ind(i):(x_ind(i+1)-1),y_ind(j):(y_ind(j+1)-1)) = ...
                slam.sensor.Sensor.logOdds2Pd(sum(theGrid.occupancyGrids{i,j}.L,3));
            
            end
        end
    end
    
else
    
    Pd  = 0.5*ones(length(theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(1),theGrid.regionOfInterest.sub_y_ind(1)}.x(1): ...
        theGrid.param.dx:theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(end),theGrid.regionOfInterest.sub_y_ind(end)}.x(end)), ...
        length(theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(1),theGrid.regionOfInterest.sub_y_ind(1)}.y(1): ...
        theGrid.param.dx:theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(end),theGrid.regionOfInterest.sub_y_ind(end)}.y(end)));%0.5*ones(length(theGrid.x)-1,length(theGrid.y)-1);
    x   = theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(1),theGrid.regionOfInterest.sub_y_ind(1)}.x(1): ...
        theGrid.param.dx:theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(end),theGrid.regionOfInterest.sub_y_ind(end)}.x(end);%theGrid.x(1:end-1);
    y   = theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(1),theGrid.regionOfInterest.sub_y_ind(1)}.y(1): ...
        theGrid.param.dx:theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(end),theGrid.regionOfInterest.sub_y_ind(end)}.y(end);%theGrid.y(1:end-1);
    z   = theGrid.regionOfInterest.z_siz;%theGrid.z(1:end-1);
    
    for i = 1:length(theGrid.regionOfInterest.sub_y_ind) %1:size(theGrid.occupancyGrids,1)
        for j = 1:length(theGrid.regionOfInterest.sub_x_ind) %1:size(theGrid.occupancyGrids,2)
            if theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(j),theGrid.regionOfInterest.sub_y_ind(i)}.init==1
                
                %we need to compute the indeces in the theGrid for subgrid i
                %             Pd(x_ind(i):(x_ind(i+1)-1),y_ind(j):(y_ind(j+1)-1)) = ...
                %                 slam.sensor.Sensor.logOdds2Pd(sum(theGrid.occupancyGrids{i,j}.L,3));
                Pd((j-1)*50+1:j*50,(i-1)*50+1:i*50) = ...
                    slam.sensor.Sensor.logOdds2Pd(sum(theGrid.occupancyGrids{theGrid.regionOfInterest.sub_x_ind(j),theGrid.regionOfInterest.sub_y_ind(i)}.L,3));
                
            end
        end
    end
    
end

%if struct output
if nargout == 1
    
    S.x = x;
    S.y = y;
    S.z = z;
    S.Pd = Pd;
    Pd = S;
end
