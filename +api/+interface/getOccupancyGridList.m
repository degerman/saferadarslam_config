function [Pd,x,y,z] = getOccupancyGridList(theGrid,fullMap,th)

if nargin<3
    fullMap = 0;
    th = 0.01;    
end

Pd  = [];
x   = [];
y   = [];
z   = [];

if fullMap == 1
    start_i = 1;
    start_j = 1;
    end_i = size(theGrid.occupancyGrids,1);
    end_j = size(theGrid.occupancyGrids,2);
else
    if isempty(theGrid.regionOfInterest.sub_y_ind) || isempty(theGrid.regionOfInterest.sub_x_ind) 
       start_i = 0;
       start_j = 0;
       end_i = 0;
       end_j = 0;
    else
        start_i = theGrid.regionOfInterest.sub_x_ind(1);
        start_j = theGrid.regionOfInterest.sub_y_ind(1);
        end_i = theGrid.regionOfInterest.sub_x_ind(end);
        end_j = theGrid.regionOfInterest.sub_y_ind(end);
    end
end

for i = start_i:end_i %1:size(theGrid.occupancyGrids,1)
    for j = start_j:end_j %1:size(theGrid.occupancyGrids,2)
        
        if theGrid.occupancyGrids{i,j}.init==1
            
            ind     = find(theGrid.occupancyGrids{i,j}.L>th);
            siz     = size(theGrid.occupancyGrids{i,j}.L);
            [x_ind,y_ind,z_ind] = ind2sub(siz,ind);
            
            if ~isempty(x_ind)
                Pd  = [Pd,slam.sensor.Sensor.logOdds2Pd(theGrid.occupancyGrids{i,j}.L(ind)')];
                x   = [x,theGrid.occupancyGrids{i,j}.x(x_ind)];
                y   = [y,theGrid.occupancyGrids{i,j}.y(y_ind)];
                z   = [z,theGrid.occupancyGrids{i,j}.z(z_ind)];
            end
        end
    end

end

%if struct output
if nargout == 1
    
    S.N = length(x);
    S.x = x;
    S.y = y;
    S.z = z;
    S.Pd = Pd;
    Pd = S;
end