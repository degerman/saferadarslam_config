classdef RadarFileReaderNewR1<slam.i_o.DataReader
    
    
    properties
        
        radar_detection_file
        
        All
        UTC_time
        
    end
    
    methods
        
        function self = RadarFileReaderNewR1(radar_detection_file,time_offset)
            
            if nargin==2
                self.UTC_time_offset = time_offset;
            else
                self.UTC_time_offset = 0;
            end
            
            self.radar_detection_file = radar_detection_file;
            
            %Get Radar measurements
            load(self.radar_detection_file);
            
            for i=1:length(detection_all_data)
                self.UTC_time(i) = detection_all_data{i}.EGO_time;
                if isfield(detection_all_data{i},'PolarPlotAll')
                    self.All{i}.PolarPlotAll = detection_all_data{i}.PolarPlotAll;
                    self.All{i}.ampl = detection_all_data{i}.ampl;
                else
                    self.All{i} = {};
                end
            end
            
            self.index      = 1;
            self.N          = length(self.All);
            
        end
                        
        function radar_meas = getNextMeasurement(self,sensor)
            
            if ~isfield(self.All{self.index},'PolarPlotAll')
                range   = [];
                u       = [];
                v       = [];
                doppler = [];
                SNR     = [];
            else
                range   = self.All{self.index}.PolarPlotAll{1}(1,:);
                u       = -self.All{self.index}.PolarPlotAll{1}(2,:); %the classic minus sign
                v       = self.All{self.index}.PolarPlotAll{1}(3,:);
                doppler = self.All{self.index}.PolarPlotAll{1}(4,:);
                SNR     = self.All{self.index}.ampl';
                SNR     = slam.sensor.Sensor.mag2db(SNR)+3; %add 3 for incoherent sum
                SNR(SNR>30) = 30;
%                SNR     = 2*SNR + 10;
             %   SNR     = cell2mat(self.All{self.index}.ampl_all{1});
            end
            
            ind     = abs(v)<0.3 & range>4 & SNR>8;
            range   = range(ind)*1.1;
            u       = u(ind);
            v       = v(ind)*0.4;
            doppler = doppler(ind);
            SNR     = SNR(ind);
            SNR     = ones(size(SNR))*20;
      
            current_time = self.UTC_time(self.index) + ...
                           self.UTC_time_offset;
            radar_meas = slam.sensor.RadarMeasurement(current_time,range,u,v,doppler,SNR,[],sensor.param.sensor_id);
                        
            self.index  = self.index + 1;
                                    
        end
                
        function UTC_time = getUTCfromDirectory(self,raw_data_directory)
            
            %get UTC from radar log
            files = dir(fullfile(self.directory,raw_data_directory,'*.conf'));
            UTC_time = zeros(1,length(files));
            
            self.index = 1;
            self.N = length(files);
            
            for i = 1:length(files)
                
                fid         = fopen(fullfile(self.directory,raw_data_directory,[num2str(i),'.conf']),'r');
                C           = textscan(fid, '%s');
                UTC_time(i) = str2double(C{1}{19});
                fclose(fid);
                
            end
            
        end        
                
    end   
    
end

