classdef RadarReader<i_o.DataReader
    
    
    properties
        
        jar_file
        adress
        
        ctx
        socket1
        
    end
    
    methods
        
        function self = RadarReader(jar_file,adress)
            
            self.jar_file = jar_file;
            self.adress = adress;
            
            javaclasspath(jar_file);
            
            import org.zeromq.*
            
            %set pub socket for subscibe
            self.ctx     = ZMQ.context(1);
            
            self.socket1  = self.ctx.socket(ZMQ.SUB);
            
            self.socket1.setConflate(1);
            self.socket1.setRcvHWM(1);
            
            self.socket1.connect(adress);
            self.socket1.subscribe('');
            self.socket1.setReceiveTimeOut(1e1);
            
            %max number of measurements
            self.N = 1e9;
            
            
        end
        function data = flush(self)
            self.socket1.recv(1);
            data = self.socket1.recv(1);
            for i=1:1000
                self.socket1.recv(1);
                data_new = self.socket1.recv(1);
                if isempty(data_new)
                    break;
                end
                data = data_new;
            end
        end
        function [meas,msg_meta] = getNextMeasurement(self,sensor)
            no_meas = true;
            i = 1;
            while no_meas && i<2
                try
                    %                msg_meta = jsondecode(native2unicode(self.socket1.recv(1))');
                    %                 msg_meta = self.socket1.recv(1);
                    msg_data = (self.socket1.recv(1))';
                    %            jsondecode(msg_meta)
                    if isempty(msg_data) || msg_data(1) == 0
                        meas = 'empty';
                    else
                        [m,~] = i_o.ksonDecode(msg_data);
                        %remove large v-values
                        ind = abs(double(m.afl_V))<0.2 & abs(double(m.afl_U))<0.75;
                        meas = slam.sensor.RadarMeasurement(double(m.fl_CurrTime),...
                            double(m.afl_Range(ind)), ...
                            double(-m.afl_U(ind)),...
                            double(m.afl_V(ind)), ...
                            double(m.afl_RangeRate(ind)),...
                            double(m.aul_SNR(ind)), [],sensor.param.sensor_id);
                        self.index  = self.index + 1;
                    end
                    
                    no_meas = false;
                catch e
                    %                 disp(e)
                    meas = 'empty';
                    msg_meta = 'empty';
                end
                i = i + 1;
            end
        end
        
    end
end

