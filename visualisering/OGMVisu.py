# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from bbox import Bbox
from kson import Kson
from pyqtgraph import Vector
import pyqtgraph as pg
import pyqtgraph.opengl as gl
import numpy as np
import argparse
import sys
import math
import zmq
import time
import os
np.set_printoptions(threshold=np.nan)

class Window(QMainWindow):
    def __init__(self):
        super(Window, self).__init__() 
        self.setStyleSheet("QMainWindow {background: 'black';}")
        self.setWindowTitle("SLAM Visualization") 
        self.setWindowState(Qt.WindowMaximized)
        
        grid_layout = QGridLayout() 
        self.w = gl.GLViewWidget(self)
        self.w.setGeometry(120,0,1800,1020)
        grid_layout.addWidget(self.w, 0, 2)
        
        self.w2 = QLabel('red', self)
        self.w2.setStyleSheet("color: white;")
        font = QFont()
        font.setPointSize(5)
        self.w2.setFont(font)
        grid_layout.addWidget(self.w2, 0, 1)
        self.w2.setGeometry(0,0,120,120)         
                              
        self.show()
        self.setZMQ()
        self.initPlot()

    def setZMQ(self):   
        self.ctx = zmq.Context()
        self.client = self.ctx.socket(zmq.SUB)
        self.client.setsockopt(zmq.SUBSCRIBE, b'') 
        self.client.setsockopt(zmq.RCVTIMEO, 200)
        self.client.setsockopt(zmq.RCVHWM, 1)
        self.client.setsockopt(zmq.CONFLATE,1)
        self.client.connect ("tcp://localhost:7585")   

    def RGB(self,input):
        tmp = np.arange(0,1,1/input)
        red = np.empty(input)
        green = np.empty(input)
        blue = np.empty(input)        
        for i in range(input):
            red[i] = min(max(max(2-tmp[i]*4,tmp[i]*4-4),0),1) #min(max(max(2-tmp[i]*5,tmp[i]*5-4),0),1)
            blue[i] = min(max(tmp[i]*4-2,0),1) #min(max(tmp[i]*5-2,0),1)
            green[i] = min(max(min(tmp[i]*4-1,4-tmp[i]*4),0),1) #min(max(min(tmp[i]*5-1,4-tmp[i]*5),0),1)      
        return np.array([np.flip(red,axis=0),np.flip(green,axis=0),np.flip(blue,axis=0),np.ones(input)]) 

    def setPoles(self):
        poleheight = 1
        poleradius = 0.5
        poleinnerradius = 0.4
        poles = []
        md = gl.MeshData.cylinder(rows=100, cols=100, radius=[0.1, 0.1], length=1)
        for i in range(len(self.message['Meas']['x'])):
            poles.append(gl.GLMeshItem(meshdata=md,color=[0.3,0.3,0.3,0.5],shader='edgeHilight')) 
            self.w.addItem(poles[i])
            #poles[i].scale(poleradius/10,poleradius/10,poleheight/10)
            #poles[i].rotate(self.message['Local']['yaw'][0]*(180/np.pi),0,0,10,local=False)
            poles[i].translate(self.message['Meas']['x'][i],self.message['Meas']['y'][i],0,local=False)   
        self.pole_init = 1
        print('pole_init = 1')         


    def initEgo(self):
        self.egowidth = 2
        self.egolength = 5
        self.egoheight = 1.5
        if 1:
            width = 1
            ego = np.zeros((10,10,10,4),dtype=np.ubyte)
            loc = np.zeros((10,10,10,4),dtype=np.ubyte)
            for i in range(10):
                for j in range(10):
                    for k in range(10):
                        if ((i > 10-width-1 or i < width) and (j > 10-width-1  or j < width) or ((i > 10-width-1 or i < width)
                            and (k > 10-width-1  or k < width)) or ((j > 10-width-1 or j < width) and (k > 10-width-1  or k < width))):
                            ego[i,j,k] = [255,0,0,255] #[255,0,0,255]
                            loc[i,j,k] = [0,255,0,255]
                        else:
                            ego[i,j,k] = [255,0,0,0] #[0,0,0,0]
                            loc[i,j,k] = [0,255,0,0]
            self.v = gl.GLVolumeItem(ego,sliceDensity=10)
            self.w.addItem(self.v)
            self.v.setGLOptions('translucent')
            self.v.hide()
            self.l = gl.GLVolumeItem(loc,sliceDensity=10)
            self.w.addItem(self.l)
            self.l.setGLOptions('translucent')
            self.l.hide()
        else:
            self.vertarr[0:4] = np.column_stack((Bbox.getBoxCornPosEgo([0,0],self.egolength,self.egowidth,0,0),np.repeat(0,4)))
            self.vertarr[4:8] = np.column_stack((Bbox.getBoxCornPosEgo([0,0],self.egolength,self.egowidth,0,0),np.repeat(self.egoheight,4)))       
            #bot
            self.facearr[0] = ([int(0),int(1),int(2)])
            self.facearr[6] = ([int(0),int(2),int(3)])
            #y = 0
            self.facearr[1] = ([int(0),int(1),int(5)])
            self.facearr[7] = ([int(0),int(5),int(4)])            
            #x = 0
            self.facearr[2] = ([int(3),int(0),int(4)])
            self.facearr[8] = ([int(3),int(4),int(7)])    
            #y = 1
            self.facearr[3] = ([int(2),int(3),int(7)])
            self.facearr[9] = ([int(2),int(7),int(6)])            
            #x = 1
            self.facearr[4] = ([int(1),int(2),int(6)])
            self.facearr[10] = ([int(1),int(6),int(5)]) 
            #top
            self.facearr[5] = ([int(4),int(5),int(6)])
            self.facearr[11] = ([int(4),int(6),int(7)])
            
            self.v = gl.GLMeshItem(vertexes=self.vertarr, faces=self.facearr, faceColors=np.tile([1,0,0,1],(12,1)), smooth=True, shader='balloon', glOptions='opaque')
            self.w.addItem(self.v)
            self.v.hide()

            self.l = gl.GLMeshItem(vertexes=self.vertarr, faces=self.facearr, faceColors=np.tile([1,0,0,1],(12,1)), smooth=True, shader='edgeHilight', glOptions='opaque')
            self.w.addItem(self.l)
            self.l.hide()

    def initPlot(self):                
        self.cols = self.RGB(100)
        self.zcols = np.arange(-1,2,0.03) 
        self.dist = 50 #camera
        self.elev = 25 
  
        self.OGM2Dxold = 0
        self.OGM2Dyold = 0
        self.OGM2Dzold = 0  
        self.OGM2Dyawo = 0
        self.elapsed_kson = 0
        self.elapsed_plot = 0
        self.elapsed_tot = 0
        self.scaleOGM2D = 0.2
        self.t = time.time()
        self.imgctr = 0
        self.localtrajectory_init = 0
        self.egotrajectory_init = 0
        self.pole_init = 0
        
        self.vertarr = np.ones((8,3))
        self.facearr = np.ones((12,3),dtype=np.int8)
        self.colorarr = np.ones((12,4))
        initimg = np.zeros((100,100,4))
        self.gridcolor = np.asarray([255,255,255,255])
        
        self.m1 = gl.GLScatterPlotItem(pos=np.asarray([1,1,1]))
        self.w.addItem(self.m1) 

        self.m3 = gl.GLImageItem(data=initimg)
        self.m3.scale(self.scaleOGM2D,self.scaleOGM2D,1)
        self.w.addItem(self.m3) 
        self.initEgo()

        self.m4 = gl.GLScatterPlotItem(pos=np.asarray([1,1,1]))
        self.w.addItem(self.m4)
        self.m4.hide()

        self.m5 = gl.GLScatterPlotItem(pos=np.asarray([0,0,0]),color=[0,0,1,1])
        self.w.addItem(self.m5)
        self.m5.hide()

        self.m6 = gl.GLLinePlotItem(pos=np.asarray([[0,0,0],[1,1,1]]),color=[1,0,0,1],width=1,mode='line_strip')
        self.w.addItem(self.m6)
        self.m6.hide()

        self.m7 = gl.GLLinePlotItem(pos=np.asarray([[0,0,0],[1,1,1]]),color=[0,1,0,1],width=1,mode='line_strip')
        self.w.addItem(self.m7)
        self.m7.hide()

    
    def set2Dgrid(self):        
        self.grid = np.asarray(self.message['OGM2D']['Pd'])
        self.grid = self.grid.reshape(len(self.message['OGM2D']['y']),len(self.message['OGM2D']['x']))
        self.grid = self.grid.T
        idx = self.grid == 0.5
        self.grid = self.grid[:,:,np.newaxis]
        self.grid = (1-self.grid)*self.gridcolor
        self.grid[idx,3] = 0
        self.m3.translate((self.message['OGM2D']['x'][0]-self.OGM2Dxold), (self.message['OGM2D']['y'][0]-self.OGM2Dyold), 0,local=False)
        self.OGM2Dxold = self.message['OGM2D']['x'][0]
        self.OGM2Dyold = self.message['OGM2D']['y'][0]
        self.m3.setData(data=self.grid)
        self.m3.update()        
    
    def setGrid(self):
        data = np.column_stack((self.message['OGM']['x'],self.message['OGM']['y'],self.message['OGM']['z']))
        sizev = np.asarray(self.message['OGM']['Pd'])-0.45
        sizev = sizev*0.5
        zcols = np.tile(self.zcols,[len(self.message['OGM']['z']), 1]).T
        zcols = np.argmin(abs(zcols-self.message['OGM']['z']),axis=0)  
        color = self.cols[:,zcols.astype(int)].T
        self.m1.setData(pos=data,color=color,size=sizev,pxMode=False)
        self.m1.setGLOptions('opaque')
        self.l.hide()
        self.m4.hide()
        self.m7.hide()
        self.localtrajectory_init = 0

    def setLocal(self):
        self.m4.show()
        data = np.column_stack((self.message['Local']['xp'],self.message['Local']['yp'],self.message['Local']['zp']))
        sizev = np.asarray(self.message['Local']['Pd'])*100
        self.m4.setData(pos=data,color=[1,1,1,0.2],size=sizev,pxMode=False)
        self.m4.setGLOptions('additive')
        self.l.show()
        self.l.resetTransform()
        self.l.scale(self.egolength/10,self.egowidth/10,self.egoheight/10)
        self.l.rotate(self.message['Local']['yaw'][0]*(180/np.pi),0,0,10,local=False)
        x = self.message['Local']['x'][0]-np.cos(self.message['Local']['yaw'][0])+np.sin(self.message['Local']['yaw'][0])
        y = self.message['Local']['y'][0]-np.sin(self.message['Local']['yaw'][0])-np.cos(self.message['Local']['yaw'][0])
        self.l.translate(x,y, 0,local=False)
        self.m7.show()
        if self.localtrajectory_init == 0:
            self.localtrajectory = np.vstack([self.message['Local']['x'][0],self.message['Local']['y'][0],0]).transpose()
            self.localtrajectory_init = 1
            self.egotrajectory_init = 0
        else:
            self.localtrajectory = np.append(self.localtrajectory,[[self.message['Local']['x'][0],self.message['Local']['y'][0],0]],axis=0)
        while len(self.localtrajectory) > 50:
            self.localtrajectory = np.delete(self.localtrajectory,0,axis=0)
        self.m7.setData(pos=self.localtrajectory)
                      
    def setEgo(self):
        self.v.show()
        self.v.resetTransform()
        self.v.scale(self.egolength/10,self.egowidth/10,self.egoheight/10)
        self.v.rotate(self.message['Ego']['yaw'][0]*(180/np.pi),0,0,1,local=False)
        x = self.message['Ego']['X'][0]-np.cos(self.message['Ego']['yaw'][0])+np.sin(self.message['Ego']['yaw'][0])
        y = self.message['Ego']['Y'][0]-np.sin(self.message['Ego']['yaw'][0])-np.cos(self.message['Ego']['yaw'][0])
        self.v.translate(x,y, 0,local=False)
        self.m6.show()
        if self.egotrajectory_init == 0:
            self.egotrajectory = np.vstack([self.message['Ego']['X'][0],self.message['Ego']['Y'][0],0]).transpose()
            self.egotrajectory_init = 1
        else:
            self.egotrajectory = np.append(self.egotrajectory,[[self.message['Ego']['X'][0],self.message['Ego']['Y'][0],0]],axis=0)
        while len(self.egotrajectory) > 50:
            self.egotrajectory = np.delete(self.egotrajectory,0,axis=0)
        self.m6.setData(pos=self.egotrajectory)

    def setMeas(self):
        self.m5.show()
        data = np.column_stack((self.message['Meas']['x'],self.message['Meas']['y'],self.message['Meas']['z']))
        sizev = np.asarray(self.message['Meas']['Pd'])
        self.m5.setData(pos=data,color=[1,0,1,1],size=sizev,pxMode=False)
        self.m5.setGLOptions('opaque')

    def setCam(self):           
        pos = pg.Vector(self.message['Ego']['X'][0],self.message['Ego']['Y'][0],self.message['Ego']['Z'][0])
        azi = self.message['Ego']['yaw'][0]*(180/np.pi)+180
        self.w.setCameraPosition(pos=pos,distance=self.dist,azimuth=azi,elevation=self.elev)
        
    def settxt(self):
        stri =("\nplot = %s [s]" % round(self.elapsed_plot,4) +
               "\ntotal = %s [s]" % round(self.elapsed_tot,4))
        self.w2.setText(stri)

    def capture(self):
        if self.enableCapture == '-c':
            img = self.w.grabFrameBuffer()
            if self.imgctr == 0:
                self.savepath = '/home/saferadar/vis/%s' % time.strftime('%y%m%d_%H%M%S')
                os.makedirs(self.savepath)
            self.imgctr += 1 
            img.save(self.savepath + '/img%s.png' % self.imgctr, "png")
            # ffmpeg -r 2 -f image2 -i /home/saferadar/vis/img%d.png -r 24 -vb 20M mapping.avi
                      
    def updatePlot(self):
        if 'OGM' in self.message.keys():                                 
            self.setGrid()
            self.capture()
        if 'OGM2D' in self.message.keys():    
            self.set2Dgrid()
        if 'Local' in self.message.keys():
            self.setLocal()
            self.capture()
        if 'Ego' in self.message.keys():         
            self.setEgo()
            self.setCam()     
        if 'Meas' in self.message.keys() and self.pole_init == 1:
            self.setMeas()  
        if 'Meas' in self.message.keys() and self.pole_init == 0:
            self.setPoles()
        self.settxt()      
        #self.capture()  

    def running(self,capture): 
        self.enableCapture = capture       
        while True:
            try:                
                self.mRec = self.client.recv()
            except zmq.ZMQError:                
                print("No message %s" % time.clock()) 
                QApplication.processEvents()    
                continue 
            self.elapsed_tot = time.time()-self.t
            self.t = time.time() 
            self.message = Kson.Msgdecode(self.mRec,'OGM') 
            self.elapsed_kson = time.time()-self.t            
            try: 
                self.updatePlot()
                QApplication.processEvents()
            except (KeyError):
                print("ValueError, IndexError, KeyError")
                QApplication.processEvents()
                continue
            self.elapsed_plot = time.time()-self.elapsed_kson-self.t            
            
def main():   
    app = QApplication(sys.argv)
    GUI = Window()
    GUI.show()
    if len(sys.argv) > 1: 
        GUI.running(sys.argv[1])
    else:
        GUI.running(sys.argv[0])   
    sys.exit(app.exec_())

main() 
    
