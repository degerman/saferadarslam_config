# -*- coding: utf-8 -*-
import numpy as np
import sys
import math
import struct
import time  

class Kson():        
    def Msgdecode(mRec,func):      
        NrOfBytesMessageLen = 8
        NrOfBytesMessageID = 1
        NrOfBytesNrOfFields = 2
        NrOfBytesBytesPerField = 2
        NrOfBytesBytesPerRnC = 4
        NrOfDicts = int.from_bytes(mRec[0:1], byteorder='little', signed=False)    
        message = {} 
        startidx = 1
        endidx = 1
        for h in range(NrOfDicts):
            message[Kson.StructNames(h,func)] = {}
            endidx += NrOfBytesMessageLen
            startidx = endidx
            endidx += NrOfBytesMessageID
            startidx = endidx
            endidx += NrOfBytesNrOfFields
            NrOfFields = int.from_bytes(mRec[startidx:endidx], byteorder='little', signed=False)
            startidx = endidx
            Fieldnames = []
            DatatypeID = np.zeros(NrOfFields,dtype=int)
            NrOfRows = np.zeros(NrOfFields,dtype=int)
            NrOfCols = np.zeros(NrOfFields,dtype=int)
            FieldNameLenIdx = startidx
            startidx += NrOfFields*NrOfBytesBytesPerField
            endidx += NrOfFields*NrOfBytesBytesPerField
            for i in range(NrOfFields):
                endidx += int.from_bytes(mRec[FieldNameLenIdx+i*NrOfBytesBytesPerField:FieldNameLenIdx+(i+1)*NrOfBytesBytesPerField] , byteorder='little', signed=False)
                Fieldnames.append(mRec[startidx:endidx].decode('utf-8'))          
                startidx = endidx
            for j in range(NrOfFields):  
                DatatypeID[j]=int.from_bytes(mRec[startidx+j:startidx+(j+1)] , byteorder='little', signed=False) 
                NrOfRows[j]=int.from_bytes(mRec[startidx+j*NrOfBytesBytesPerRnC+NrOfFields:startidx+(j+1)*NrOfBytesBytesPerRnC+NrOfFields] , byteorder='little', signed=False) 
                NrOfCols[j]=int.from_bytes(mRec[startidx+j*NrOfBytesBytesPerRnC+NrOfFields+NrOfFields*NrOfBytesBytesPerRnC:startidx+(j+1)*NrOfBytesBytesPerRnC+NrOfFields+NrOfFields*NrOfBytesBytesPerRnC] , byteorder='little', signed=False)
            startidx = endidx = startidx+NrOfFields*NrOfBytesBytesPerRnC+NrOfFields+NrOfFields*NrOfBytesBytesPerRnC   
            for k in range(NrOfFields):
                datatype = Kson.dataTypes(DatatypeID[k])
                length = NrOfRows[k]*NrOfCols[k]
                data = struct.unpack('%s'% length +Kson.decodeTypes(datatype),mRec[startidx:endidx+NrOfRows[k]*NrOfCols[k]*datatype])
                startidx = endidx = endidx+(NrOfRows[k]*NrOfCols[k])*datatype
                if 'field1' in Fieldnames[k]:
                    message.pop(Kson.StructNames(h,func),None)
                else:   
                    message[Kson.StructNames(h,func)][Fieldnames[k]] = data
        return message
    def dataTypes(id):
        dict = {1:8,2:4,3:1,4:1,5:2,6:2,7:4,8:4,9:8,10:8,11:1,12:1}
        return dict[id]
    
    def decodeTypes(id):
        dict = {8:'d',4:'f',2:'H',1:'B'}
        return dict[id]
    
    def StructNames(id,func):
        if func is 'OGM':
            id += 10
        dict = {0:'Obj',1:'ClusBox',2:'ClusBoxUpdt',3:'Ego',4:'Sensor',5:'Ref',6:'MainTrack',7:'Det',8:'DetParam',9:'SS',10:'OGM',11:'OGM2D',12:'Ego',13:'Local',14:'Meas'}
        return dict[id]


   
