# -*- coding: utf-8 -*-
import numpy as np
import sys
import math
import struct
from PyQt5 import QtCore
   
class Bbox():    
    def getBoxCornPos(cornPos,l,w,h,cornNrMeas):
        cornData = np.zeros([2,4])
        for cornNr in range(4):
            cornData[:,cornNr]  = Bbox.cornMap(cornPos[0],cornPos[1],0,h,w,l,cornNrMeas,cornNr+1,0,0,0,0,0)
        return np.transpose(cornData)

    def getBoxCornPosEgo(cornPos,l,w,h,cornNrMeas):
        cornData = np.zeros([2,4])
        for cornNr in range(4):
            cornData[:,cornNr]  = Bbox.cornMap(cornPos[0],cornPos[1],0,h,w,l,cornNrMeas,cornNr+1,0,0,0,0,1)
        return np.transpose(cornData)

    def getBoxCornPosQP(cornPos,l,w,h,cornNrMeas):
        cornData = np.zeros([2,4],dtype=np.float32)
        for cornNr in range(4):
            cornData[:,cornNr]  = Bbox.cornMap(cornPos[0],cornPos[1],0,h,w,l,cornNrMeas,cornNr+1,0,0,0,0,1)
        return [QtCore.QPointF(cornData[0,0],cornData[1,0]), QtCore.QPointF(cornData[0,1],cornData[1,1]), QtCore.QPointF(cornData[0,2],cornData[1,2]), QtCore.QPointF(cornData[0,3],cornData[1,3]),]    
    
    # def getBoxCornPosQP2(cornPos,l,w,h,cornNrMeas):
    #     cornData = np.zeros([2,4],dtype=np.float32)
    #     for cornNr in range(4):
    #         cornData[:,cornNr]  = Bbox.cornMap(cornPos[0],cornPos[1],0,h,w,l,cornNrMeas,cornNr+1,0,0,0,0,0)
    #     return [QtCore.QPointF(cornData[0,0],cornData[1,0]), QtCore.QPointF(cornData[0,1],cornData[1,1]), QtCore.QPointF(cornData[0,2],cornData[1,2]), QtCore.QPointF(cornData[0,3],cornData[1,3]),]  
   
    def cornMap(x,y,v,h,w,l,cornNrIn,cornNrOut,xe,ye,ve,he,displace):
        midData = Bbox.getCornData(x,y,0,h,-w,-l,cornNrIn,0,0,0,0,0,0)
        midPos = midData[0:2]
        if displace == 1:
            cornDataOut = Bbox.getCornData(midPos[0],midPos[1],v,h,w,l,cornNrOut,xe,ye,ve,he,1,1)
        else:
            cornDataOut = Bbox.getCornData(midPos[0],midPos[1],v,h,w,l,cornNrOut,xe,ye,ve,he,0,0)
        return cornDataOut

    def getCornData(x,y,v,h,w,l,cornNr,xe,ye,ve,he,dx,dy):
        s = Bbox.cornSign(cornNr)
        s1 = s[0][0]
        s2 = s[0][1]
        s3 = s[1][0]
        s4 = s[1][1]
        xc = x+s1*np.cos(h)*l/2+s2*np.sin(h)*w/2+np.cos(h)*dx-np.sin(h)*dy
        yc = y+s3*np.sin(h)*l/2+s4*np.cos(h)*w/2+np.sin(h)*dx+np.cos(h)*dy
        cornPos = np.append([xc],[yc],axis=0)
        return cornPos
    
    def cornSign(cornNr):
        if cornNr ==1:
            s = [[-1, 1],[-1, -1]]
        elif cornNr == 2:
            s = [[1, 1],[1, -1]]
        elif cornNr == 3:
            s = [[1, -1],[1, 1]]
        elif cornNr == 4:
            s = [[-1, -1],[-1, 1]]
        else:
            s = [[0, 0],[0, 0]]
        return s
