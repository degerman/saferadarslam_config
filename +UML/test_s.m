%%

VISUALIZE = false;

if ispc
    path    = 'C:\svn\radarsens\2_TechnicalWork\1_Simulation\RadarSimulator3D\ScanDataLandscape_RFS5_1\';
else
    path    = '/Users/johandegerman/data/radar/sauer_sp/ScanDataTCtrk01_2_2/';
%    path    = '/Users/johandegerman/data/radar/sauer_sp/Hedelf_11_RFS3_1_IMUrot1_ext/';
end

scenario = saferadar.api.Scenario(path);

param   = slam.util.readJsonFile(['+slam' filesep '+sensor' filesep 'radar_param.json']);

param.sensor.pos = [0,0,0]';

radar   = slam.sensor.RadarSensor(param.sensor,[]);

param_GRID  = struct('limits',[0 100 -5 5 -1 1],...
    'dx',0.1,...
    'dy',0.1,...
    'dz',0.1,...
    'sub_dx',10,...
    'sub_dy',10,...
    'sub_dz',5);

GRID  = slam.grid.TheGrid(param_GRID);

radarGrid = slam.grid.RadarMeasurementGrid(radar, param.measurement_grid);

iter = 1;

while scenario.has_next()
    
    det = scenario.next(true);
    
    ego_state = scenario.ego.distribution.mean;

    currentTime = det.time;
    range = det.data.det_rng;
    u    = det.data.det_uv(1,:);
    v    = det.data.det_uv(2,:);
    doppler = det.data.det_speed;
    
    SNR = 50*ones(size(range));
    
    radar_meas = slam.sensor.RadarMeasurement(currentTime,range,u,v,doppler,SNR,[]);
    
    pos  = ego_state(1:3);
    vel  = ego_state(4:6);
    speed = norm(vel);
    angle = [atan2(vel(2),vel(1)),0,0]';
    angle_rate = [0,0,0]';
    
    ego_meas = slam.localization.EgoState(pos,angle,speed,angle_rate);
    
    radarGrid.update(radar_meas);
    bounding_box = radarGrid.getBoundingBox(ego_meas);
    GRID.ROI.update(bounding_box);
    GRID.update(radarGrid,ego_meas);
    
    sprintf('Mapping iteration: %d',iter)
    iter=iter+1;
    
end

GRID.visualize3D([0.1,1,2,5,10],[GRID.limits(1,:), GRID.limits(2,:), GRID.limits(3,:)])

%%
gridMap     = GRID.convert2C();
scenario = saferadar.api.Scenario(path);
logger = slam.util.DataLogger();

nrParticles = 500;
particleFilter = slam.localization.ParticleFilter(nrParticles, gridMap);

if VISUALIZE
    visualizer = slam.util.Visualizer(GRID,nrParticles);
end

motionModel = slam.localization.FullMotionModel(radar.dt);


det = scenario.next(true);
ego_state = scenario.ego.distribution.mean;

pos     = ego_state(1:3);
vel     = ego_state(4:6);
speed = norm(vel);
angle = [atan2(vel(2),vel(1)),0,0]';
angle_rate = [0,0,0]';
ego_meas = slam.localization.EgoState(pos,angle,speed,angle_rate);

particleFilter.init(ego_meas,motionModel,{radar});

if VISUALIZE
    visualizer.init(particleFilter);
end

iter = 1;

while scenario.has_next()
    
    det = scenario.next(true);

    ego_state = scenario.ego.distribution.mean;

    currentTime = det.time;
    range = det.data.det_rng;
    u    = det.data.det_uv(1,:);
    v    = det.data.det_uv(2,:);
    doppler = det.data.det_speed;
    
    SNR = 50*ones(size(range));
    
    radar_meas = slam.sensor.RadarMeasurement(currentTime,range,u,v,doppler,SNR,[]);
    
    pos     = ego_state(1:3);
    vel     = ego_state(4:6);
    speed   = norm(vel);
    angle   = [atan2(vel(2),vel(1)),0,0]';
    angle_rate = [0,0,0]';
    ego_meas = slam.localization.EgoState(pos,angle,speed,angle_rate);    
 
    particleFilter.updateMeasurements({radar_meas},ego_meas);
    
    particleFilter.evaluateWeights();
    
    particleFilter.stateEstimation();
    
    particleFilter.resample();
    
    particleFilter.updateAllParticles();
    
    logger.putData([particleFilter.getEstimate().pos;particleFilter.getEstimate().angle]);
    logger.putReference([ego_meas.pos;ego_meas.angle]);
                
    if VISUALIZE
        visualizer.update(particleFilter,logger.data,logger.ground_truth);
    end
    
    sprintf('Localization iteration: %d',iter)
    iter=iter+1;

end

logger.plotError
logger.plotErrorLongLat