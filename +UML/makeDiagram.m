function makeDiagram(classNames)
% makeAnimalDiagram creates a UML class diagram for the provided example
%
% See also:
%   UMLgui ClassFile Tree ClassGroup
%
% Ben Goddard 12/12/13

% change this directory list to create custom scripts

FANCY = true;

%    addpath('/Users/johandegerman/code/git/saferadarslam/');
if nargin==0
    classNames{1} = 'slam.grid.TheGrid';
    classNames{2} = 'slam.grid.OccupancyGrid';
%    classNames{3} = 'slam.grid.SensorGrid';
%    classNames{4} = 'slam.grid.RadarGrid';
%    classNames{5} = 'slam.grid.LidarGrid';
%    classNames{6} = 'slam.grid.RegionOfInterest';
end

% strart

nFiles = length(classNames);
fileList = {};
completeTree = true;
md=[];
nonParentClass = [];


% establish which are valid classes
for iFile = 1:nFiles
    className = classNames{iFile};
    metaDataCmd = ['md  = ?' className ';'];
    
    try
        eval(metaDataCmd);
        
    catch err
        
        if(strcmp(err.identifier, 'MATLAB:class:InvalidSuperClass'))
            completeTree = false;
            nonParentClass = className;
        else
            fprintf(1,'Unknown error:\n');
            fprintf(1,[err.message '\n']);
        end
    end
    
    if(~isempty(md) && completeTree)
        fileList = [fileList className];      %#ok
    end
end

% put into alphabetical order
[~,sortOrder] = sort(lower(fileList));
fileList = fileList(sortOrder);

if(~completeTree)
    fileList = {};
end

if(isempty(fileList))
    
    if(completeTree)
        fprintf(1,'Selected directories contain no class files!\n');
    else
        fprintf(1,['Incomplete tree: ' nonParentClass ' missing superclasses\n']);
    end
    
else
    
    % determine if we use fancy (true) or compact (false) plotting
    options.fancy=true;
    
    % make and open the pdf
    tree = UML.Tree(fileList,options);
    saveFile = tree.saveFile;
    
    open(saveFile);
    
end

